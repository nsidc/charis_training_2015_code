
# coding: utf-8

# <h1>The charistools hypsometry class</h1>

# <h2>Purpose of this notebook:</h2>
# 
# <p>This notebook will introduce you to:
# <ol>
# <li>the charistools class for storing hypsometry data (data "by elevation band")</li>
# <li>pandas (http://pandas.pydata.org), "Python Data Analysis Library," a python library for working with spreadsheet-types of data</li>
# <li>reading and writing hypsometry data</li>
# </ol>
# 
# <p>At the end of this lesson, you should be able to read and write hypsometry data and understand this simple format.</p>

# Let's begin with a few configuration things: tell matplotlib to display plots right here in the notebook, and tell python I want to use numpy (for numerical array types), matplotlib (for making nice plots), and pandas (for working with hypsometry data).
# 
# Configure pandas display options to display big arrays nicely.

# In[ ]:

get_ipython().magic(u'pylab notebook')
from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
pd.set_option('display.max_rows', 370)
pd.set_option('display.max_columns', 70)
pd.set_option('display.width', 200)


# <p>CHARIS hypsometry files are ASCII-formatted files we defined for this project.</p>
# 
# <p>Some sample hypsometry-format files are included on the data drives, in main_training/data/derived_hypsometries.  Like we did with fixed_filenames and forcing_filenames, we can get to them using the ModelEnv class.</p>
# 
# <p>Take a look at the 'hypsometry' level in in the ModelEnv class:</p>

# In[ ]:

# Mac/linux:
get_ipython().magic(u'more /Users/brodzik/2016_Almaty_training/modis_tiles_config.ini')
# Windows:
#%more E:/2016_Almaty_training/modis_tiles_config.ini


# <p>For now, we're going to read the snow_on_land_by_elevation data.  There are several file patterns described there.  Eventually we will work with all of them, but for this exercise we'll just work with snow_on_land_by_elevaction. </p>
# 
# <p>Initialize myEnv as a modelEnv object for this config file and set your topDir to the CHARIS data directory on your machine:</p>

# In[ ]:

from charistools.modelEnv import ModelEnv
configFile = '/Users/brodzik/2016_Almaty_training/modis_tiles_config.ini'
topDir = '/Users/brodzik/projects/CHARIS_FTP_copy/main_training/data'
myEnv = ModelEnv(tileConfigFile=configFile, topDir=topDir, verbose=True)


# <p>Look at the data I included on the thumbdrive.  Find the file for snow_on_land_by_elevation:</p>

# In[ ]:

get_ipython().magic(u'ls /Users/brodzik/projects/CHARIS_FTP_copy/main_training/data/derived_hypsometries')


# <p>Use help to figure out how to call myEnv.hypsometry_filename:</p>

# In[ ]:

#help(myEnv)
help(myEnv.hypsometry_filename)


# <p>So for snow_on_land_by_elevation, we need to know the drainageID, the year and the contour size in meters.</p>
# 
# <p>The drainageID that I used for the sample files is "IN_Hunza_at_Danyour" and I created them at 100 m contour levels for data from 2001.  The default value for contour_m is already 100 so we don't need to worry about that.  Get the filename like this:</p>

# In[ ]:

SOLFile = myEnv.hypsometry_filename(type='snow_on_land_by_elevation',
                                    drainageID='IN_Hunza_at_Danyour',
                                    year=2001)
SOLFile


# <p>Look at the file with more.</p>
# 
# We are storing these data as ASCII files, since they are relatively small (much smaller dat volume than the MODIS snow or albedo data) and not complex.  ASCII is easy to read and edit.  The file format is simple:
# 
# <ol>
# <li> comments
# <li> number of columns
# <li> elevation at each column
# <li> data records with date and 1 value for each column
# </ol>
# 
# <p>In python, we're reading the data into a pandas object to take advantage of pandas data manipulation and routines for plotting time series and the data.</p>

# In[ ]:

get_ipython().magic(u'more /Users/brodzik/projects/CHARIS_FTP_copy/main_training/data/derived_hypsometries/IN_Hunza_at_Danyour.2001.0100m.snow_on_land_area_by_elev.txt')


# <p>Use the charistools Hypsometry class to read the data in this file into python:</p>

# In[ ]:

from charistools.hypsometry import Hypsometry
SOL = Hypsometry(filename=SOLFile)


# In[ ]:

help(SOL)


# So this SOL variable now contains a python object that includes two "attributes", a string array with the comments (one comment line each), and a thing called a pandas "DataFrame".
# 
# Pandas DataFrame terminology:
# <ul>
#     <li> rows and columns can be referred to by name as well as integers (like in a matrix)
#     <li> df.index : the names of the rows (hypsometry.py makes these dates)
#     <li> df.columns : the names of the columns (hypsometry.py makes these elevation bands)
# </ul>
#     

# In[ ]:

SOL.comments


# In[ ]:

SOL.data


# <p>Get the names of rows with the pandas "index" attribute (one row for each date):</p>

# In[ ]:

SOL.data.index 
#SOL.data.index.tolist()  # equivalently: list( SOL.data.index )
#help( SOL.data.index)


# <p>Get summary statistics for the whole array</p>

# In[ ]:

SOL.data.describe()


# <p>Or just "head" (first 5 rows) of the data:</p>

# In[ ]:

SOL.data.head()


# <p>Find out the names of the data columns.</p>

# In[ ]:

SOL.data.columns


# <p>Various ways to get a value or small subset of values:</p>

# In[ ]:

SOL.data.ix['2001-01-05','1500.0']


# In[ ]:

SOL.data.ix['2001-01-05']['1500.0':'1600.0']


# <h2>Printing sca for specific dates</h2>

# To slice hypsometry data by rows (a range of dates):

# In[ ]:

SOL.data.loc['2001-01-01':'2001-01-05']


# To see every 30 days:

# In[ ]:

SOL.data[::30]


# To just look at a single column (this will return a pandas "Series" object, basically an index and value for every entry):

# In[ ]:

SOL.data['1400.0']


# <h2>Make a subset and save it as a new hypsometry file.</h2>

# In[ ]:

subset = SOL.data.loc['2001-01-01':'2001-01-31']
subset


# In[ ]:

snow_jan = Hypsometry(data=subset, comments=['Subset of snow on land for Hunza', 'Jan 2001'])
snow_jan.print()


# In[ ]:

snow_jan.write('/Users/brodzik/tmp/training/snow_jan_by_elev.txt')


# In[ ]:

get_ipython().magic(u'more /Users/brodzik/tmp/training/snow_jan_by_elev.txt')


# <p>Pandas is a versatile format for tabular data.  It was developed by a math whiz working on Wall Street to analyze financial data quickly.  It's very powerful, we have only scratched the surface using it for melt modelling.</p>
# 
# <ol>
# <li>http://pandas.pydata.org - the pandas web page</li>
# <li>this book has been very helpful for me:  McKinney, Wes.  "Python for Data Analysis" <img src="../images/McKinney_cover.png" width="200"></li>
# </ol>
# 
# <p>Like many powerful tools, you will only really learn it by working with it yourself.  It is much more powerful than working with spreadsheets in Excel. I encourage you to explore what it can help you do whenever you are working with tabular data.</p>

# In[ ]:



