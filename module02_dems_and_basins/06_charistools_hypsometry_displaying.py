
# coding: utf-8

# <h1>The charistools hypsometry class: displaying hypsometry data</h1>

# <h2>Purpose of this notebook:</h2>
# 
# <p>This notebook will introduce you to:
# <ol>
# <li>displaying a plot of annual hypsometry information</li>
# <li>displaying an image of annual snow cover hypsometry data</li>
# </ol>
# 
# <p>At the end of this lesson, you should be able to visualize hypsometry data as a plot or as an image, like this:
# 
# <img src="../images/sample_hypsometry_snow.png" height=400>
# 
# 

# In[ ]:

get_ipython().magic(u'pylab notebook')
from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
pd.set_option('display.max_rows', 370)
pd.set_option('display.max_columns', 70)
pd.set_option('display.width', 200)


# <p>Read the sample snow_on_land_by_elevation data.  </p>

# In[ ]:

from charistools.hypsometry import Hypsometry
from charistools.modelEnv import ModelEnv
configFile = '/Users/brodzik/2016_Almaty_training/modis_tiles_config.ini'
topDir = '/Users/brodzik/projects/CHARIS_FTP_copy/main_training/data'
myEnv = ModelEnv(tileConfigFile=configFile, topDir=topDir, verbose=True)


# In[ ]:

SOLFile = myEnv.hypsometry_filename(type='snow_on_land_by_elevation',
                                    drainageID='IN_Hunza_at_Danyour',
                                    year=2001)

SOL = Hypsometry(filename=SOLFile)
SOL.data


# <h2>Just look at snow_on_land at selected elevations in this basin:</h2>

# In[ ]:

elevations = ['1400.0','2400.0','3400.0','4400.0','5400.0']
SOL.data[elevations]


# In[ ]:

SOL.data[elevations].plot()


# But notice how the elevations go from highest to lowest, where you'd really want to see lowest to highest, like this:

# In[ ]:

elevations[::-1]


# In[ ]:

SOL.data[elevations[::-1]].plot()


# Or you might not want them all on the same plot, maybe separate plots, sharing a y axis range would be better:

# In[ ]:

SOL.data[elevations[::-1]].plot( title="Basin snow-on-land area at selected elevations",
                                subplots=True, sharey=True, figsize=(12,8) )


# <h2>Total the snow-on-land area by date and display basin snow-on-land time series</h2>

# I want to do this so often, I wrote a method on the hypsometry Class that will total up each row (date) for all elevations and return a total for each day:

# In[ ]:

SOL_by_doy = SOL.data_by_doy()
SOL_by_doy.head()


# In[ ]:

fig, ax = plt.subplots(1,1)
SOL_by_doy.plot( title='Hunza snow-on-land area, 2001', figsize=(8,6) )


# Here is a diagram showing the three main parts of a figure in matplotlib.pyplot:
# 
# <img src="../images/pyplot_figure_parts.png" height=300>
# 
# For more options on matplotlib line styles, see http://matplotlib.org/1.3.1/examples/pylab_examples/line_styles.html

# In[ ]:

fig, ax = plt.subplots(1,1)
ax.set_title('Hunza Snow-on-Land, 2001')
ax.set_ylabel('Area ($km$)')
ax.set_ylim([0,7500])
plt.subplots_adjust(bottom=0.15)
# plot style cheatsheet:
# first character specifies color:
#   'b' : blue
#   'g' : green
#   'r' : red
#   'c' : cyan
#   'm' : magenta
#   'y' : yellow
#   'k' : black
# next 1 or 2 characters specify line style:
#   '_' : short horizontal lines
#   '-' : solid line (default)
#   '--' : dashed line
#   ':' : fine dots
#   'o' : solid circles
#   '.' : bigger dots 
SOL_by_doy.plot( ax=ax, style='r--') # here is another comment


# In[ ]:

# Get xy values from position display that appears when moving mouse over the plot
ax.annotate('Possible snow event in late July?',
            xy=('2001-07-29',2200),  # data coordinate x, y for arrow to point to
            xytext=(0.05, 0.05),    # fraction of figure from bottom left 0.0, 0.0
            textcoords='figure fraction',
            arrowprops=dict(facecolor='red', shrink=0.05),
            color='b')


# <p>Save the figure in my training notes folder:</p>

# In[ ]:

# change filename to .pdf to save to different format
fig.savefig('/Users/brodzik/tmp/training/SOL_notes.png', dpi=150 )


# <h2>A plot along a row can be plotted as a real hypsometry (with elevations on y-axis and data values on x-axis)</h2>

# So we can turn this around by just asking for a horizontal bar plot:

# In[ ]:

fig, ax = plt.subplots(1,1)
SOL.data.ix['2001-01-31'].describe()
SOL.data.ix['2001-01-31'].plot( title='Basin Snow-on-land area by elevation, 2001-01-31', 
                                   kind='barh', figsize=(8,6))


# <p>But the y axis tick labels are really busy, so we can subset them with matplotlib functions that operate on the "axes" object.</p>
# 
# <p>And we can set X and Y axis labels:</p>

# In[ ]:

fig, ax = plt.subplots(1,1)
ax.set_title('Basin Snow-on-land area by elevation, 2001-01-31')
ax.set_xlabel('SCA (sq km)')
ax.set_ylabel('Elevation (m)')
SOL.data.ix['2001-01-31'].plot( kind='barh', figsize=(8,8))
ax.set_yticks( ax.get_yticks()[::10] )
ax.set_yticklabels( SOL.data.columns[::10] )


# <p>All of the built-in matplotlib functions are available, too:</p>

# In[ ]:

fig, ax = plt.subplots(1,1)
ax.set_title('Basin Change in Snow-on-land, Jan to Jun')
ax.set_xlabel('January Area (sq km)')
ax.set_ylabel('June Area (sq km)')
#ax.set_aspect('equal')
#ax.set_xlim( [ 0, 600 ] )
#ax.set_ylim( [ 0, 600 ] )
#plt.subplots_adjust(bottom=0.15)

plt.scatter(SOL.data.ix['2001-01-31'], SOL.data.ix['2001-06-30'] )


# <h2>Use imshow to display information about all elevations for the whole year</h2>

# In[ ]:

SOL.data


# In[ ]:

fig, ax = plt.subplots()
ax.imshow(SOL.data)


# In[ ]:

fig, ax = plt.subplots()
ax.imshow(SOL.data, aspect='auto')


# <p>It would probably be more intuitive to display elevations on the y axis and time on the x axis</p>
# 

# <p>This can be a common way to look at Hypsometry data, so I made it a method in the Hypsometry class:</p>

# In[ ]:

help(SOL)


# In[ ]:

SOL.imshow()


# <p>Put all this together for a multiple-panel visualization of SOL data.</p>
# <p>Try making 2 panels, with: 1) total daily SOL as a line plot, 2) the imshow data for the whole year by elevation</p>
# <p>What if you just wanted to look at the data for July 15 to August 15, to look at that snow event?</p>
# 

# In[ ]:



