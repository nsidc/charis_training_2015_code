
# coding: utf-8

# <h1>The charistools hypsometry class: displaying hypsometry data</h1>

# <h2>Purpose of this notebook:</h2>
# 
# <p>This notebook will introduce you to:
# <ol>
# <li>displaying a line plot of annual hypsometry data</li>
# <li>displaying a barplot of hypsometry data on a given data (or annually)</li>
# <li>displaying an image of annual snow cover hypsometry data at all elevations</li>
# </ol>
# 
# <p>At the end of this lesson, you should be able to visualize hypsometry data as a plot or as an image, like this:
# 
# <img src="../images/Hunza_hyps_2panel.png" width="500">
# 
# 

# In[ ]:

get_ipython().magic(u'pylab notebook')
from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
pd.set_option('display.max_rows', 370)
pd.set_option('display.max_columns', 70)
pd.set_option('display.width', 200)


# <p>Read the sample snow_on_land_by_elevation data.  </p>

# In[ ]:

from charistools.hypsometry import Hypsometry
from charistools.modelEnv import ModelEnv
configFile = '/Users/brodzik/2016_Almaty_training/modis_tiles_config.ini'
topDir = '/Users/brodzik/projects/CHARIS_FTP_copy/main_training/data'
myEnv = ModelEnv(tileConfigFile=configFile, topDir=topDir, verbose=True)


# In[ ]:

SOLFile = myEnv.hypsometry_filename(type='snow_on_land_by_elevation',
                                    drainageID='IN_Hunza_at_Danyour',
                                    year=2001)

SOL = Hypsometry(filename=SOLFile)
SOL.data


# <h2>Just look at snow_on_land at selected elevations in this basin:</h2>

# In[ ]:

elevations = ['1400.0','2400.0','3400.0','4400.0','5400.0']
SOL.data[elevations]


# In[ ]:

SOL.data[elevations].plot()


# But notice how the elevations go from highest to lowest, where you'd really want to see lowest to highest, like this:

# In[ ]:

elevations[::-1]


# In[ ]:

SOL.data[elevations[::-1]].plot()


# Or you might not want them all on the same plot, maybe separate plots, sharing a y axis range would be better:

# In[ ]:

SOL.data[elevations[::-1]].plot( title="Basin snow-on-land area at selected elevations",
                                subplots=True, sharey=True, figsize=(8,6) )


# <h2>Total the snow-on-land area by date and display basin snow-on-land time series</h2>

# I want to do this so often, I wrote a method on the hypsometry Class that will total up each row (date) for all elevations and return a total for each day:

# In[ ]:

SOL_by_doy = SOL.data_by_doy()
SOL_by_doy.head()


# In[ ]:

fig, ax = plt.subplots(1,1)
SOL_by_doy.plot( title='Hunza snow-on-land area, 2001', figsize=(8,6) )


# Here is a diagram showing the three main parts of a figure in matplotlib.pyplot:
# 
# <img src="../images/pyplot_figure_parts.png" height=300>
# 
# For more options on matplotlib line styles, see http://matplotlib.org/1.3.1/examples/pylab_examples/line_styles.html

# In[ ]:

fig, ax = plt.subplots(1,1)
ax.set_title('Hunza Snow-on-Land, 2001')
ax.set_ylabel('Area ($km$)')
ax.set_ylim([0,7500])
plt.subplots_adjust(bottom=0.15)
# plot style cheatsheet:
# first character specifies color:
#   'b' : blue
#   'g' : green
#   'r' : red
#   'c' : cyan
#   'm' : magenta
#   'y' : yellow
#   'k' : black
# next 1 or 2 characters specify line style:
#   '_' : short horizontal lines
#   '-' : solid line (default)
#   '--' : dashed line
#   ':' : fine dots
#   'o' : solid circles
#   '.' : bigger dots 
SOL_by_doy.plot( ax=ax, style='r--') # here is another comment


# In[ ]:

# Get xy values from position display that appears when moving mouse over the plot
ax.annotate('Possible snow event in late July?',
            xy=('2001-07-29',2200),  # data coordinate x, y for arrow to point to
            xytext=(0.05, 0.05),    # fraction of figure from bottom left 0.0, 0.0
            textcoords='figure fraction',
            arrowprops=dict(facecolor='red', shrink=0.05),
            color='b')


# <p>Save the figure in my training notes folder:</p>

# In[ ]:

# change filename to .pdf to save to different format
fig.savefig('/Users/brodzik/tmp/training/SOL_notes.png', dpi=150 )


# <h2>A plot along a row can be plotted as a real hypsometry (with elevations on y-axis and data values on x-axis)</h2>

# <p>To plot data with values on x-axis and elevations on y-axis:</p>

# In[ ]:

fig, ax = plt.subplots(1,1)
SOL.data.ix['2001-01-31'].describe()
SOL.data.ix['2001-01-31'].plot( title='Basin Snow-on-land area by elevation, 2001-01-31', 
                                   kind='barh', figsize=(8,6))


# <p>But the y axis tick labels are really busy, so we can subset them with matplotlib functions that operate on the "axes" object.</p>
# 
# <p>And we can set X and Y axis labels:</p>

# In[ ]:

fig, ax = plt.subplots(1,1)
ax.set_title('Basin Snow-on-land area by elevation, 2001-01-31')
ax.set_xlabel('SCA (sq km)')
ax.set_ylabel('Elevation (m)')
SOL.data.ix['2001-01-31'].plot( kind='barh', figsize=(8,8))
ax.set_yticks( ax.get_yticks()[::10] )
ax.set_yticklabels( SOL.data.columns[::10] )


# <p>The charistools Hypsometry class has a method to do this barplot for you:</p>

# In[ ]:

help(Hypsometry)


# In[ ]:

fig, ax = plt.subplots(1,2)

ax[0] = SOL.barplot(ax[0], '2001-01-31', title='SOL, 2001-01-31', xlabel='SOL ($km^2$)', 
             ylabel='Elevation ($m$)', verbose=False)
ax[1] = SOL.barplot(ax[1], '2001-06-30', title='SOL, 2001-06-30', xlabel='SOL ($km^2$)', 
             ylabel='Elevation ($m$)', verbose=False)


# <p>Notice the x-axis limits are not the same for both plots.  Python gives you the ability to edit any of the plot attributes for either of the axes.  So you can reset the xlimit so the comparison makes more sense.</p>
# 
# <p>And calling "tight_layout" on the fig object will redraw the current figure so labels don't overlap.</p>

# In[ ]:

ax[1].set_xlim(0,400)
fig.tight_layout()


# <p>All of the built-in matplotlib plotting functions are available, too:</p>

# In[ ]:

fig, ax = plt.subplots(1,1)
ax.set_title('Basin Change in Snow-on-land, Jan to Jun')
ax.set_xlabel('January Area (sq km)')
ax.set_ylabel('June Area (sq km)')
#ax.set_aspect('equal')
#ax.set_xlim( [ 0, 600 ] )
#ax.set_ylim( [ 0, 600 ] )
#plt.subplots_adjust(bottom=0.15)

plt.scatter(SOL.data.ix['2001-01-31'], SOL.data.ix['2001-06-30'] )


# <h2>Use imshow to display information about all elevations for the whole year</h2>

# In[ ]:

SOL.data


# In[ ]:

fig, ax = plt.subplots()
ax.imshow(SOL.data)


# In[ ]:

fig, ax = plt.subplots()
ax.imshow(SOL.data, aspect='auto')


# <p>It would probably be more intuitive to display elevations on the y axis and time on the x axis.  Use numpy "rot90" function to rotate the data array:</p>
# 

# In[ ]:

ax.imshow(np.rot90(SOL.data), aspect='auto')


# <p>We can make label ticks with the actual dates and elevations by using the extent paramter for imshow.</p>
# 
# <p>First, get the dates:</p>
# 

# In[ ]:

SOL.data.index


# <p>The imshow "extent" paramter is a 4 element list of beginning/ending data values on each axis.  The dates in the index are "datetime64" types, so we have to do some conversions to include them in "extent" limits:
# </p>

# In[ ]:

# Create xlims as sequence of floats for each date
import matplotlib.dates as mdates
x_values = mdates.date2num(SOL.data.index.to_pydatetime())
x_values


# <p>Grab the first [0] and last [-1] dates and elevations (columns) for the limits:</p>

# In[ ]:

x_lims = mdates.date2num([SOL.data.index[0].to_pydatetime(), 
                          SOL.data.index[-1].to_pydatetime()])
y_lims = [float(SOL.data.columns[0]), 
          float(SOL.data.columns[-1])]
extent= [x_lims[0], x_lims[1], y_lims[0], y_lims[1]]
extent


# <p>Now we can use these limits to get meaningful labels on the imshow display.</p>

# In[ ]:

fig, ax = plt.subplots(1,1)
ax.imshow(np.rot90(SOL.data.values), aspect='auto', extent=extent, cmap='Greens')
ax.set_title('Hunza Snow on Land Area')
ax.set_ylabel('Elevation (m)')


# <p>But wait, what about the labels on the X axis?  They should be dates, and there are a few extra steps to make them format nicely:</p>
# 

# In[ ]:

fig, ax = plt.subplots(1,1)
ax.imshow(np.rot90(SOL.data.values), aspect='auto', extent=extent, cmap='Greens')
ax.set_title('Hunza Snow on Land Area')
ax.set_ylabel('Elevation (m)')
ax.xaxis_date()
date_format = mdates.DateFormatter('%b')
ax.xaxis.set_major_formatter(date_format)


# <p>Finally, a word about the default rainbow color scheme.  It's terrible!  Don't use it!  Read this short article about how bad it is for people who are color-blind!</p>
# 
# <pre>
# Light, A. and P. J. Bartlein. 2004.  "The End of the Rainbow?  Color Schemes for 
# Improved Data Graphics.  EOS, 85(40).
# </pre>
# 
# <p>We could spend a whole workshop talking about how to use color in scientific presentations.  For now, I'm just going to choose the monochrome "Greens" colormap.</p>
# 
# <p>This method of displaying matrix data as an image can be a useful way to look at Hypsometry data, so I made it a method in the Hypsometry class:</p>

# In[ ]:

help(SOL.imshow)


# In[ ]:

fig, ax = plt.subplots(1,1)
ax = SOL.imshow(ax, title="Hunza Snow on Land Area", cmap='Greens')


# <p>Put all this together for a multiple-panel visualization of SOL data.</p>
# <p>Try making 2 panels, with: 1) total daily SOL as a line plot, 2) the imshow data for the whole year by elevation</p>

# In[ ]:




# <p>What if you just wanted to look at the data for July 15 to August 15, to look at that snow event?</p>
# <p>If you want to change the dateFormat string, see this page for options:</p>
# 
# http://www.tutorialspoint.com/python/time_strptime.htm
# 
# </p>

# In[ ]:

fig, ax = plt.subplots(2,1)
ax[0].set_title('Hunza basin Snow-on-Land, 2001')
ax[0].set_ylabel('Area ($km^2$)')
ax[0].set_ylim([0,7500])
SOL_by_doy.plot( ax=ax[0], style='g')
ax[1] = SOL.imshow(ax[1], title="Hunza Snow on Land Area by elevation", cmap='Greens')
fig.savefig('/Users/brodzik/tmp/training/Hunza_hyps_2panel.png')


# In[ ]:

#SOL.data = SOL.data['2001-07-15':'2001-08-15']
#fig, ax = plt.subplots()
#ax = SOL.imshow(ax, title="Hunza Snow on Land Area", cmap='Greens', dateFormat='%m-%d')


# In[ ]:



