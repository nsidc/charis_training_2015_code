
# coding: utf-8

# <h2>Purpose of this notebook:</h2>
# 
# <p>This notebook will introduce you to:
# <ol>
# <li>displaying the CHARIS DEM tiles</li>
# <li>clipping the DEM clipped to a certain drainage basin</li>
# <li>using the DEM and drainageID masks derived from a shapefile to create a Hypsometry of a basin </li>
# </ol>
# 
# <p>At the end of this lesson, you should be able to look at DEM data, clip it, and convert it to a hypsometry for a given basin.</p>

# In[ ]:

get_ipython().magic(u'pylab notebook')
from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt


# In[ ]:

from charistools.modelEnv import ModelEnv
configFile = '/Users/brodzik/2016_Almaty_training/modis_tiles_config.ini'
topDir = '/Users/brodzik/projects/CHARIS_FTP_copy/main_training/data'
myEnv = ModelEnv(tileConfigFile=configFile, topDir=topDir, verbose=True)


# In[ ]:

get_ipython().magic(u'more /Users/brodzik/2016_Almaty_training/modis_tiles_config.ini')


# <p>You should be able to tell that we've classified this as a fixed input file, and that you need a tileID to get a complete filename</p>

# In[ ]:

demFile = myEnv.fixed_filename(type='dem', tileID='h24v05')
demFile


# <p>Don't forget to read the 00notes.txt file:</p>

# In[ ]:

get_ipython().magic(u'more /Users/brodzik/projects/CHARIS_FTP_copy/main_training/data/srtmgl3/00notes.txt')


# In[ ]:

from charistools.readers import read_tile
dem = read_tile(demFile)
np.shape(dem)


# In[ ]:

np.amin(dem), np.amax(dem)


# <h3>Display one tile of the DEM</h3>

# In[ ]:

fig, ax = plt.subplots()
ax.imshow(dem, cmap="Greys", vmin=np.amin(dem), vmax=np.amax(dem), interpolation='None')
ax.set_title('CHARIS DEM, h24v05')
plt.axis('off')


# <p>You can also select certain elevations to display and filter away the rest using simple masking operations.</p>
# 

# In[ ]:

tmp = dem
tmp[tmp < 3500] = 0


# In[ ]:

fig, ax = plt.subplots()
ax.imshow(tmp, cmap="Greys", vmin=np.amin(dem), vmax=np.amax(dem), interpolation='None')
ax.set_title('CHARIS DEM < 3500, h24v05')
plt.axis('off')


# <h3>Exercise</h3>
# 
# <p>How would you display 2 DEM tiles, say for h23v05 and h24v05?</p>
# 
# <p>Try doing it two different ways, once with subplots(1,2) and once by reading the tiles and putting them together into a large, 2400x4800, array (Hint: look up help for the numpy "concatenate" function).  Is one or the other way easier?  Why? </p>
# 
# <p>If the default "Greys" color map seems strange to you, try reversing it with "Greys_r".  All matplotlib color maps are available in the reverse order by appending "_r"</p>

# In[ ]:




# <h3>Exercise</h3>
# 
# <p>The Hunza basin spans the two tiles h23v05 and h24v05.  Try to "clip" the DEMS for these two tiles with the basin_mask files.  (Hint: use the basin_mask to filter the DEM locations outside the mask.) </p>
# 
# 
# 

# <h3>Use the charistools.convertor tool to create the hypsometry for the Hunza</h3>
# 
# <p>The MODIS tile format is really nice for storing information in files with predictable sizes, so it helps manage files and limit how much memory we need for any given operation.  However, as you found in the previous exercises, it gets very cumbersome for dealing with basin data that spans 2 (or more) tiles.  I have written a charistools method called "Dem2Hypsometry" that will do this work for you, given a "drainageID" that describes a set of tile basin_masks.  The routine is one of a family routines that all work the same way, figuring out which tiles are needed and then only reading a one set of tiles at a time, to minimize the memory needed for these kinds of operations.</p>
# 
# <p>Here is the help message:</p>

# In[ ]:

from charistools.convertors import Dem2Hypsometry
help(Dem2Hypsometry)


# <p>So using the default contour level of 100 m, we can create a Hypsometry for any drainage with basin_mask tiles in our modelEnv.</p>
# 
# <p>The routine finds the basin_mask files that modelEnv points to, and uses those tileIDs to read the DEM data and produce the Hypsometry.  I've included masks for IN_Hunza_at_Danyour.  But if you create your own drainage masks from your shapefile and put them here, you can also run this step using your drainageID string.</p>

# In[ ]:

get_ipython().magic(u'ls /Users/brodzik/projects/CHARIS_FTP_copy/main_training/data/basin_masks')


# In[ ]:

dem_hyps = Dem2Hypsometry(drainageID="IN_Hunza_at_Danyour", modelEnv=myEnv, verbose=True)


# In[ ]:

dem_hyps.print()


# <p>Since DEM data aren't daily data, note that the index is just 'NoDate', so we can get the Series information like this:</p>

# In[ ]:

dem_hyps.data.ix['NoDate']


# In[ ]:

fig, ax = plt.subplots(1,1)
ax = dem_hyps.barplot(ax, index='NoDate',
                      title='Hunza Area by elevation', 
                      xlabel = 'Area ($km^2$)')


# <h2>Exercise</h2>
# 
# <p>If you produced your own basin shapefile during the GIS training, try to use the basin_masks for your basin to produce a hypsometry plot like we did for the Hunza.</p>
# 
# <p>If you did not produce your own basin shapefile, try working with someone near you who has a different basin.</p>

# In[ ]:



