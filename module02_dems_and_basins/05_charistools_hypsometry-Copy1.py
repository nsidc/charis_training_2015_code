
# coding: utf-8

# <h1>The charistools hypsometry class</h1>

# <h2>Purpose of this notebook:</h2>
# 
# <p>This notebook will introduce you to:
# <ol>
# <li>the charistools class for storing hypsometry data (data "by elevation band")</li>
# <li>pandas (http://pandas.pydata.org), a python module for working with spreadsheet-types of data</li>
# <li>reading and writing hypsometry data</li>
# <li>displaying a plot of annual hypsometry information</li>
# <li>displaying an image of annual snow cover hypsometry data</li>
# </ol>
# 
# <p>At the end of this lesson, you should be able to read and write hypsometry data and display it like this:
# 
# <img src="../images/sample_hypsometry_snow.png" height=400>
# 
# 

# Let's begin with a few configuration things: tell matplotlib to display plots right here in the notebook, and tell python I want to use numpy (for numerical array types), matplotlib (for making nice plots), and pandas (for working with hypsometry data).
# 
# Configure pandas display options to display big arrays nicely.

# In[ ]:

get_ipython().magic(u'pylab notebook')
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
pd.set_option('display.max_rows', 370)
pd.set_option('display.max_columns', 70)
pd.set_option('display.width', 200)


# <p>CHARIS hypsometry files are ASCII-formatted files we defined for this project.</p>
# 
# <p>Some sample hypsometry-format files are included on the data drives, in main_training/data/derived_hypsometries.  Like we did with fixed_filenames and forcing_filenames, we can get to them using the ModelEnv class.</p>
# 
# <p>Take a look at the 'hypsometry' level in in the ModelEnv class:</p>

# In[ ]:

# Mac/linux:
get_ipython().magic(u'more /Users/brodzik/2016_Almaty_training/modis_tiles_config.ini')
# Windows:
#%more E:/2016_Almaty_training/modis_tiles_config.ini


# <p>For now, we're going to read the snow_on_land_by_elevation data.  </p>
# 
# <p>Initialize myEnv as a modelEnv object for this config file and set your topDir to the CHARIS data directory on your machine:</p>

# In[ ]:

from charistools.modelEnv import ModelEnv
configFile = '/Users/brodzik/2016_Almaty_training/modis_tiles_config.ini'
topDir = '/Users/brodzik/projects/CHARIS_FTP_copy/main_training/data'
myEnv = ModelEnv(tileConfigFile=configFile, topDir=topDir, verbose=True)


# <p>Use help to figure out how to call myEnv.hypsometry_filename:</p>

# In[ ]:

#help(myEnv)
help(myEnv.hypsometry_filename)


# <p>So for snow_on_land_by_elevation, we need to know the drainageID, the year and the contour size in meters.</p>
# 
# <p>The drainageID that I used for the sample files is "IN_Hunza_at_Danyour" and I created them at 100 m contour levels for data from 2001.  The default value for contour_m is already 100 so we don't need to worry about that.  Get the filename like this:</p>

# In[ ]:

SOLFile = myEnv.hypsometry_filename(type='snow_on_land_by_elevation',
                                    drainageID='IN_Hunza_at_Danyour',
                                    year=2001)
SOLFile


# <p>Look at the file with more:</p>

# In[ ]:

get_ipython().magic(u'more /Users/brodzik/projects/CHARIS_FTP_copy/main_training/data/derived_hypsometries/IN_Hunza_at_Danyour.2001.0100m.snow_on_land_area_by_elev.txt')


# <p>Use the charistools Hypsometry class to read the data in this file into python:</p>

# In[ ]:

from charistools.hypsometry import Hypsometry
SOL = Hypsometry(filename=SOLFile)


# In[ ]:

help(SOL)


# So this SOL variable now contains a python object that includes two "attributes", a string array with the comments (one comment line each), and a thing called a pandas "DataFrame".
# 
# Pandas DataFrame terminology:
# <ul>
#     <li> rows and columns can be referred to by name as well as integers (like in a matrix)
#     <li> df.index : the names of the rows (hypsometry.py makes these dates)
#     <li> df.columns : the names of the columns (hypsometry.py makes these elevation bands)
# </ul>
#     

# In[ ]:

SOL.comments


# In[ ]:

SOL.data


# Look at the names of rows with the pandas "index" attribute:

# In[ ]:

SOL.data.index 
#SOL.data.index.tolist()  # equivalently: list( SOL.data.index )
#help( SOL.data.index)


# In[ ]:

SOL.write('out.txt', verbose=True)
#sca.write( 'out.txt', verbose=True)


# In[ ]:

get_ipython().magic(u'more out.txt')


# In[ ]:

SOL.data.describe()


# In[ ]:

SOL.data.columns


# In[ ]:

SOL.data.ix['2001-01-05','1500.0']


# ## Printing sca for specific dates

# To slice hypsometry data by rows (a range of dates):

# In[ ]:

SOL.data.loc['2001-01-01':'2001-01-05']


# To see every 30 days:

# In[ ]:

SOL.data.ix[::30]


# To just look at a single column (this will return a pandas "Series" object, basically an index and value for every entry):

# In[ ]:

SOL.data['1400.0']


# <h2>Just look at snow_on_land at selected elevations in this basin:</h2>

# In[ ]:

elevations = ['1400.0','2400.0','3400.0','4400.0','5400.0']
SOL.data[elevations]


# In[ ]:

SOL.data[elevations].plot()


# But notice how the elevations go from highest to lowest, where you'd really want to see lowest to highest:

# In[ ]:

elevations[::-1]


# In[ ]:

SOL.data[elevations[::-1]].plot()


# Or you might not want them all on the same plot, maybe separate plots, sharing a y axis range would be better:

# In[ ]:

SOL.data[elevations[::-1]].plot( title="Basin snow-on-land area at selected elevations",
                                subplots=True, sharey=True, figsize=(12,8) )


# <h2>Total the snow-on-land area by date and display basin snow-on-land time series</h2>

# I want to do this so often, I wrote a method on the hypsometry Class that will total up each row (date) for all elevations and return a total for each day:

# In[ ]:

SOL_by_doy = SOL.data_by_doy()
SOL_by_doy.head()


# In[ ]:

fig, ax = plt.subplots(1,1)
SOL_by_doy.plot( title='Hunza snow-on-land area, 2001', figsize=(6,4) )


# Here is a diagram showing the three main parts of a figure in matplotlib.pyplot:
# 
# <img src="../images/pyplot_figure_parts.png" height=300>
# 
# For more options on matplotlib line styles, see http://matplotlib.org/1.3.1/examples/pylab_examples/line_styles.html

# In[ ]:

fig, ax = plt.subplots(1,1)
ax.set_title('Hunza Snow-on-Land, 2001')
ax.set_ylabel('Area ($km$)')
ax.set_ylim([0,7500])
plt.subplots_adjust(bottom=0.15)
# plot style cheatsheet:
# first character specifies color:
#   'b' : blue
#   'g' : green
#   'r' : red
#   'c' : cyan
#   'm' : magenta
#   'y' : yellow
#   'k' : black
# next 1 or 2 characters specify line style:
#   '_' : short horizontal lines
#   '-' : solid line (default)
#   '--' : dashed line
#   ':' : fine dots
#   'o' : solid circles
#   '.' : bigger dots 
SOL_by_doy.plot( ax=ax, style='r--', figsize=(8,6) ) # here is another comment


# <h2>A plot along a row can be plotted as a real hypsometry (with elevations on y-axis and data values on x-axis)</h2>

# So we can turn this around by just asking for a horizontal bar plot:

# In[ ]:

fig, ax = plt.subplots(1,1)
SOL.data.ix['2001-01-31'].describe()
SOL.data.ix['2001-01-31'].plot( title='Basin Snow-on-land area by elevation, 2001-01-31', 
                                   kind='barh', figsize=(8,6))


# In[ ]:

fig, ax = plt.subplots(1,1)
ax.set_title('Basin Snow-on-land area by elevation, 2001-01-31')
ax.set_xlabel('SCA (sq km)')
ax.set_ylabel('Elevation (m)')
SOL.data.ix['2001-01-31'].plot( kind='barh', figsize=(8,8))
ax.set_yticks( ax.get_yticks()[::10] )
ax.set_yticklabels( SOL.data.columns[::10] )


# ## All of the built-in matplotlib functions are available, too:

# In[ ]:

fig, ax = plt.subplots(1,1)
ax.set_title('Basin Change in Snow-on-land, Jan to Jun')
ax.set_xlabel('January Area (sq km)')
ax.set_ylabel('June Area (sq km)')
#ax.set_aspect('equal')
#ax.set_xlim( [ 0, 600 ] )
#ax.set_ylim( [ 0, 600 ] )
#plt.subplots_adjust(bottom=0.15)

plt.scatter(SOL.data.ix['2001-01-31'], SOL.data.ix['2001-06-30'] )


# ## Save data to an external file

# In[ ]:

fig.savefig('SOL_plot.png', dpi=300 )  # change filename to .pdf to save to different format


# <h2>Use imshow to display information about all elevations for the whole year</h2>

# In[ ]:

SOL.data


# In[ ]:

fig, ax = plt.subplots()
ax.imshow(SOL.data)


# In[ ]:

fig, ax = plt.subplots()
ax.imshow(SOL.data, aspect='auto')


# In[ ]:



