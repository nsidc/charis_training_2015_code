
# coding: utf-8

# <h1>A few plotting references</h1>

# <h3>Errors/questions/"how do I do...?"</h3>
# 
# <ol>
# <li>google is your friend!  cut-and-paste the error exactly, along with a phrase of what you were doing, e.g. "ValueError...." import matplotlib.  Chances are that someone else has had the same problem.</li>
# <li>Free book, Lin, Johnny, "A Hands-On Introduction to Using Python in the Atmostpheric and Oceanic Sciences":  
# http://www.johnny-lin.com/pyintro
# </li>
# </ol>
# 

# <h3>matplotlib stuff:</h3>
# 
# <p>Visit the matplotlib gallery to see the power of matplotlib:</p>
# 
# <p>http://matplotlib.org/gallery.html</p>

# <h3>Named colors:</h3>
# 
# http://matplotlib.org/mpl_examples/color/named_colors.hires.png
# 
# <img src="../images/named_colors.hires.png" width=800 height=800>

# <h3>Possible colormaps:</h3>
# 
# http://matplotlib.org/1.2.1/examples/pylab_examples/show_colormaps.html
# 
# <img src="../images/show_colormaps.hires.png" height="400" width="400">

# <h2>Example for setting/formatting imshow axis ticks and labels</h2>
# 
# http://stackoverflow.com/questions/23139595/dates-in-the-xaxis-for-a-matplotlib-plot-with-imshow

# Here is a diagram showing the three main parts of a figure in matplotlib.pyplot:
# 
# <img src="../images/pyplot_figure_parts.png" height=300>
# 
# For more options on matplotlib line styles, see http://matplotlib.org/1.3.1/examples/pylab_examples/line_styles.html
# 

# <h2>Good latex reference:</h2>
#     
# <p>https://en.wikibooks.org/wiki/LaTeX</p>
#     

# In[ ]:



