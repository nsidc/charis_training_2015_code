
# coding: utf-8

# <h2>Convert MOD10A1_GF raster to hyps and display</h2>

# In[ ]:

get_ipython().magic(u'pylab notebook')
from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
from charistools.modelEnv import ModelEnv
configFile = '/Users/brodzik/2016_Almaty_training/modis_tiles_config.ini'
topDir = '/Users/brodzik/projects/CHARIS_FTP_copy/main_training/data'
myEnv = ModelEnv(tileConfigFile=configFile, topDir=topDir, verbose=True)


# In[ ]:

from charistools.convertors import Fsca2Hypsometry
#snow_hyps = Fsca2Hypsometry(drainageID="IN_Hunza_at_Danyour", year=2001, modelEnv=myEnv, 
#                            verbose=True)


# In[ ]:

fscafile = '/Users/brodzik/tmp/training/IN_Hunza_at_Danyour.2001.0100m.MOD10A1_by_elev.txt'
#snow_hyps.write(filename=fscafile)
snow_hyps = Hypsometry(filename=fscafile)


# In[ ]:

help(snow_hyps.imshow)


# In[ ]:

fig, ax = plt.subplots()
ax = snow_hyps.imshow(ax, title='IN_Hunza_at_Danyour fSCA, 2001', 
            cmap='Blues', xlabel='Date', dateFormat='%b', ylabel='MOD10A1 fSCA ($km^2$)')


# In[ ]:



