
# coding: utf-8

# <h2>Loops for converting and saving MOD10A1_GF and MOD10A1 albedo raster to 3-surface hyps </h2>

# In[ ]:

get_ipython().magic(u'pylab notebook')
from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
from charistools.modelEnv import ModelEnv
configFile = '/Users/brodzik/2016_Almaty_training/modis_tiles_config.ini'
topDir = '/Users/brodzik/projects/CHARIS_FTP_copy/main_training/data'
myEnv = ModelEnv(tileConfigFile=configFile, topDir=topDir, verbose=True)


# In[ ]:

from charistools.convertors import Fsca2Hypsometry


# In[ ]:

drainageIDs = ["IN_Hunza_at_Danyour"]
years = [2001, 2004]
thresholds = [0.46, 0.56, 0.66]
ablation_types = ['snow_on_ice_by_elevation', 'exposed_glacier_ice_by_elevation']
ablation_codes = ['SOI', 'EGI']


# In[ ]:

solFile = myEnv.hypsometry_filename(drainageID=drainageIDs[0], 
                                    type='snow_on_land_by_elevation', year=years[0])
soiFile = myEnv.hypsometry_filename(drainageID=drainageIDs[0], 
                                    type='snow_on_ice_by_elevation', year=years[0],
                                    ablation_method='albedo_mod10a1',
                                    threshold=thresholds[0])
egiFile = myEnv.hypsometry_filename(drainageID=drainageIDs[0], 
                                    type='exposed_glacier_ice_by_elevation', year=years[0],
                                    ablation_method='albedo_mod10a1',
                                    threshold=thresholds[0])
print(solFile)
print(soiFile)
print(egiFile)


# In[ ]:

for drainageID in drainageIDs:
    for year in years:
        # SOL is not a function of ablation area threshold
        outfile = myEnv.hypsometry_filename(drainageID=drainageID, 
                                            type='snow_on_land_by_elevation', 
                                            year=year, verbose=True)
        by_elev = Fsca2Hypsometry(drainageID=drainageID,
                                  year=year, 
                                  fsca_area='SOL',
                                  modelEnv=myEnv, 
                                  outfile=outfile, decimal_places=2,
                                  verbose=True)
        for threshold in thresholds:
            for i in np.arange(len(ablation_types)):
                outfile = myEnv.hypsometry_filename(drainageID=drainageID, 
                                                    type=ablation_types[i], 
                                                    year=year,
                                                    ablation_method='albedo_mod10a1',
                                                    threshold=threshold,
                                                    verbose=True)
                by_elev = Fsca2Hypsometry(drainageID=drainageID,
                                          year=year,
                                          fsca_area=ablation_codes[i],
                                          threshold=threshold,
                                          modelEnv=myEnv,
                                          outfile=outfile,
                                          decimal_places=2,
                                          verbose=True)

