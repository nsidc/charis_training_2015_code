
# coding: utf-8

# <h2>Purpose of this notebook:</h2>
# 
# <p>This notebook will introduce you to:
# <ol>
# <li>using charistools to create a Hypsometry of MODIS MOD10A1 total fSCA for the Hunza basin </li>
# </ol>
# 
# <p>At the end of this lesson, you should be able to create total fSCA Hypsometry data for which you have basin_mask tiles.</p>
# 
# <img src="../images/charistools.convertors.png">

# In[1]:

get_ipython().magic(u'pylab notebook')
from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt


# In[2]:

from charistools.modelEnv import ModelEnv
configFile = '/Users/brodzik/2016_Almaty_training/modis_tiles_config.ini'
topDir = '/Users/brodzik/projects/CHARIS_FTP_copy/main_training/data'
myEnv = ModelEnv(tileConfigFile=configFile, topDir=topDir, verbose=True)


# <p>So up to this point we've been working with fixed data (in the sense of being "fixed" in time, basically with no time component), like the CHARIS DEM and MODICE data.  Next we will convert a whole year of snow cover data into Hypsometry format, using MOD10A1_GF ("gap-filled") data cubes.</p>
# 
# <p>Begin by looking at the modis_tiles_config.ini file, at the "mod10a1_gf" category:</p>

# In[3]:

get_ipython().magic(u'more /Users/brodzik/2016_Almaty_training/modis_tiles_config.ini')


# <p>You should be able to tell that we've classified mod10a1_gf (MOD10A1 "gap-filled" data) as a forcing input file, and that you need a tileID and year to get a complete filename.  Also, it's an .h5 "tile_cube," so the charistools.readers ModisTileCube interface will be useful, but you'll need to know the variable name "varname" to tell it to read. </p>

# In[4]:

snowFile = myEnv.forcing_filename(type='mod10a1_gf', tileID='h24v05', year=2001)
snowFile


# <p>For more information about these data, read the 00notes.txt file:</p>

# In[5]:

get_ipython().magic(u'more /Users/brodzik/projects/CHARIS_FTP_copy/main_training/data/mod10a1_snow_gf/00notes.txt')


# In[6]:

from netCDF4 import Dataset
f = Dataset(filename=snowFile, mode='r', format='NETCDF4')
f


# In[7]:

f.groups['500m']


# In[8]:

f.close()


# In[9]:

from charistools.readers import ModisTileCube
snow_cube = ModisTileCube(snowFile, varname='fsca')
help(snow_cube)


# In[10]:

snow = snow_cube.read(doy=31)
print(snow.shape)
print(np.amin(snow), np.amax(snow))


# <h3>Display one tile of the MOD10A1_GF data</h3>

# In[11]:

fig, ax = plt.subplots()
ax.imshow(snow, cmap="Greys_r", vmin=0.0, vmax=1.0, interpolation='None')
ax.set_title('MOD10A1_GF, h24v05, 2001-01-31')
plt.axis('off')


# <h3>Use the charistools.convertor tool to create the MOD10A1 snow cover Hypsometry for the Hunza</h3>
# 
# <p>The charistools method to do this is "Fsca2Hypsometry".  It works like Modice2Hypsometry and Dem2Hypsometry, only it will do the tile-mosaiking for your drainageID for every date in the MOD10A1_GF data cube.
# </p>
# 
# <p>Here is the help message:</p>

# In[3]:

from charistools.convertors import Fsca2Hypsometry

#help(Fsca2Hypsometry)
import charistools.convertors
help(charistools.convertors)


# <p>There are several defaults we will use: contour level of 100 m, and fsca_type='mod10a1_gf', fsca_area='total', and to begin, let's just do 3 days at the beginning of the year.
# </p>
# 
# <p>Remember that the routine finds the basin_mask files that modelEnv points to, and uses those tileIDs to read the DEM data, snow cover and produce the Hypsometry.  </p>

# In[13]:

snow_hyps = Fsca2Hypsometry(drainageID="IN_Hunza_at_Danyour", year=2001, modelEnv=myEnv, 
                           stop_doy=3, verbose=True)


# In[14]:

snow_hyps.print()


# <h3>Exercise</h3>
# 
# <p>Try running Fsca2Hypsometry for the whole year of 2001, and then use any of the Hypsometry display routines to look at the data.  Do this for drainageID="IN_Hunza_at_Danyour" or for your own basin.
# </p>

# In[ ]:



