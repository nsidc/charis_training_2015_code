
# coding: utf-8

# <h2>Convert MOD10A1_GF and MOD10A1 albedo raster to 3-surface hyps and display</h2>

# In[12]:

get_ipython().magic(u'pylab notebook')
from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
from charistools.modelEnv import ModelEnv
configFile = '/Users/brodzik/2016_Almaty_training/modis_tiles_config.ini'
topDir = '/Users/brodzik/projects/CHARIS_FTP_copy/main_training/data'
myEnv = ModelEnv(tileConfigFile=configFile, topDir=topDir, verbose=True)


# In[13]:

from charistools.convertors import Fsca2Hypsometry
#help(Fsca2Hypsometry)


# In[14]:

sol_hyps = Fsca2Hypsometry(drainageID="IN_Hunza_at_Danyour", year=2001, 
                            modelEnv=myEnv, fsca_area='SOL', verbose=True)
soi_hyps = Fsca2Hypsometry(drainageID="IN_Hunza_at_Danyour", year=2001, 
                           modelEnv=myEnv, fsca_area='SOI', verbose=True)
egi_hyps = Fsca2Hypsometry(drainageID="IN_Hunza_at_Danyour", year=2001, 
                           modelEnv=myEnv, fsca_area='EGI', verbose=True)


# In[16]:

drainageIDs = ['IN_Hunza_at_Danyour']
years = [2001, 2004]
thresholds = [0.46]
solFile = myEnv.hypsometry_filename(drainageID=drainageIDs[0], 
                                    type='snow_on_land_by_elevation', year=years[0])
soiFile = myEnv.hypsometry_filename(drainageID=drainageIDs[0], 
                                    type='snow_on_ice_by_elevation', year=years[0],
                                    ablation_method='albedo_mod10a1',
                                    threshold=thresholds[0])
egiFile = myEnv.hypsometry_filename(drainageID=drainageIDs[0], 
                                    type='exposed_glacier_ice_by_elevation', year=years[0],
                                    ablation_method='albedo_mod10a1',
                                    threshold=thresholds[0])
print(solFile)
print(soiFile)
print(egiFile)


# In[17]:

sol_hyps.data.head()


# In[18]:

soi_hyps.data.head()


# In[19]:

egi_hyps.data.head()


# In[28]:

fig, ax = plt.subplots(3,1)
ax[0] = soi_hyps.imshow(ax[0], 
                        title='IN_Hunza_at_Danyour snow-on-ice, 2001',
                        cmap='Blues', xlabel='Date', dateFormat='%b', 
                        ylabel='fSCA ($km^2$)')
ax[1] = egi_hyps.imshow(ax[1], 
                        title='IN_Hunza_at_Danyour exposed-glacier-ice, 2001',
                        cmap='Purples', xlabel='Date', dateFormat='%b', 
                        ylabel='fSCA ($km^2$)')
ax[2] = sol_hyps.imshow(ax[2], 
                        title='IN_Hunza_at_Danyour snow-on-land, 2001',
                        cmap='Greens', xlabel='Date', dateFormat='%b', 
                        ylabel='fSCA ($km^2$)')
fig.tight_layout()


# In[29]:

help(Fsca2Hypsometry)


# In[ ]:



