
# coding: utf-8

# <h2>Converting MODICE data from raster to hypsometry</h2>

# <h2>Purpose of this notebook:</h2>
# 
# <p>This notebook will introduce you to:
# <ol>
# <li>displaying the MODICE minimum snow and ice extent tiles</li>
# <li>using charistools to create a hypsometry of MODICE for a basin </li>
# </ol>
# 
# <p>At the end of this lesson, you should be able to look at MODICE data, and convert it to a hypsometry for a given basin.</p>

# In[ ]:

get_ipython().magic(u'pylab notebook')
from __future__ import print_function
from charistools.modelEnv import ModelEnv
from charistools.convertors import Modice2Hypsometry
import matplotlib.pyplot as plt


# In[ ]:

tileConfigFile = '/Users/brodzik/2016_Almaty_training/modis_tiles_config.ini'
topDir = '/Users/brodzik/projects/CHARIS_FTP_copy/main_training/data'
myEnv = ModelEnv(topDir=topDir, tileConfigFile=tileConfigFile, verbose=True )


# <p>Similar to the Dem2Hypsometry convertor, there is a Modice2Hypsometry convertor:</p>

# In[ ]:

from charistools.convertors import Modice2Hypsometry
help(Modice2Hypsometry)


# In[ ]:

modice_hyps = Modice2Hypsometry(drainageID="IN_Hunza_at_Danyour", modelEnv=myEnv, verbose=True)


# In[ ]:

modice_hyps.print()


# <p>Display the barplot of this data</p>

# In[ ]:

fig, ax = plt.subplots()
ax = modice_hyps.barplot(ax, index='NoDate', title='Hunza MODICE by elevation',
                        xlabel='Area ($km^2$)')


# <h3>Exercise (time permitting)</h3>
# 
# <p>We have included MODICE data on the thumbdrives for 1, 2 and 3 "strikes".  For 1 strike, the pixel is turned off after only one instance of modscag falling below 15% fSCA.  For n=2 or 3 strikes, the pixel must fall below 15% for 2 or more times, respectively.</p>
# 
# <p>Try reading the MODICE data for 1, 2 and/or 3 strikes, clip them to your basin of interest and compare either the images or hypsometries or both.</p>
# 
# <p>With help from Bruce: Make raster masks of glacier outlines from the GIS training and compare to MODICE 1-2-3 strikes.</p>
# 
# <p>Questions for discussion:</p>
# <ol>
# <li>How valuable is a comparison of MODICE (annual minimum snow and ice), for a minimum of 5 years from 2000-2014, to glacier outlines from a single day?</li>
# <li>Would you expect them to match exactly?  Why or why not?</li>
# <li>What are the strengths vs. limitations of glacier outlines from Landsat images?</li>
# <li>What are the strengths vs. limitations of MODICE minimum snow and ice extent?</li>
# </ol>

# In[ ]:



