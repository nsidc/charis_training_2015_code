
# coding: utf-8

# <h2>Purpose of this notebook:</h2>
# 
# <p>This notebook will introduce you to:
# <ol>
# <li>using charistools to create a Hypsometry of downscaled temperatures for the Hunza basin </li>
# </ol>
# 
# <p>At the end of this lesson, you should be able to create temperature Hypsometry data for which you have basin_mask tiles.</p>
# 
# <img src="../images/charistools.convertors.temperature.png">

# In[1]:

get_ipython().magic(u'pylab notebook')
from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
from charistools.modelEnv import ModelEnv
configFile = '/Users/brodzik/2016_Almaty_training/modis_tiles_config.ini'
topDir = '/Users/brodzik/projects/CHARIS_FTP_copy/main_training/data'
myEnv = ModelEnv(tileConfigFile=configFile, topDir=topDir, verbose=True)


# <p>Begin by looking at the modis_tiles_config.ini file, at the "temperature" category:</p>

# In[4]:

get_ipython().magic(u'more $configFile')


# <p>You should be able to tell that we've classified temperature data as a forcing input file, and that you need a tileID and year to get a complete filename.  Also, temperature is another .h5 "tile_cube," so the charistools.readers ModisTileCube interface will be useful, but you'll need to know the variable name "varname" to tell it to read. </p>

# In[5]:

tempFile = myEnv.forcing_filename(type='temperature', tileID='h24v05', year=2001)
tempFile


# <p>For more information about these data, read the 00notes.txt file:</p>

# In[ ]:

get_ipython().magic(u'more /Users/brodzik/projects/CHARIS_FTP_copy/main_training/data/temperature/00notes.txt')


# In[6]:

from netCDF4 import Dataset
f = Dataset(filename=tempFile, mode='r', format='NETCDF4')
f


# In[7]:

f.groups['500m']


# <p>So the varname we want is "tsurf"</p>

# In[8]:

f.close()


# In[9]:

from charistools.readers import ModisTileCube
temp_cube = ModisTileCube(tempFile, varname='tsurf')
help(temp_cube)


# In[10]:

temp = temp_cube.read(doy=31)
print(temp.shape)
print(np.amin(temp), np.amax(temp))


# <h3>Display one tile of the downscaled temperature data</h3>

# In[14]:

fig, ax = plt.subplots()
ax.imshow(temp, cmap="RdBu_r", vmin=273.15-30., vmax=273.15+30, interpolation='None')
ax.set_title('Downscaled ERA-Interim, h24v05, 2001-01-31')
plt.axis('off')


# <h3>Use the charistools.convertor tool to create the temperature Hypsometry for the Hunza</h3>
# 
# <p>The charistools method to do this is "Temperature2Hypsometry".  It works like Fsca2Hypsometry and will do the tile-mosaiking for your drainageID for every date in the temperature data cube.  Where Fsca2Hypsometry calculated the fSCA area in each elevation band, Temperature2Hypsometry calculates the average temperature for each elevation band.  
# </p>
# 
# <p>Temperature2Hypsometry assumes inputs are in Kelvin units, and saves the Hypsometry data in Celsius units.</p>
# 
# <p>Here is the help message:</p>

# In[15]:

from charistools.convertors import Temperature2Hypsometry
help(Temperature2Hypsometry)


# <p>We will use the default contour level of 100 m, and to begin, let's do 3 days at the beginning of the year.
# </p>

# In[17]:

temp_hyps = Temperature2Hypsometry(drainageID="IN_Hunza_at_Danyour", 
                                   year=2001, 
                                   modelEnv=myEnv, 
                                   verbose=True)


# In[ ]:

temp_hyps.print()


# <h3>Exercise</h3>
# 
# <p>Try running Temperature2Hypsometry for the whole year of 2001, and then use any of the Hypsometry display routines to look at the data.  Do this for drainageID="IN_Hunza_at_Danyour" or for your own basin.
# </p>

# In[19]:

fig, ax = plt.subplots()
ax = temp_hyps.imshow(ax=ax, title='Hunza dscaled temperatures, 2001', 
                          cmap='RdBu_r', vmin=-40, vmax=40)
plt.show()


# In[ ]:



