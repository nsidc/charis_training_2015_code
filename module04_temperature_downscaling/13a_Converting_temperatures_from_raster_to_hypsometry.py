
# coding: utf-8

# <h2>Example running Temperature2Hypsometry for Hunza, 2001</h2>

# In[1]:

get_ipython().magic(u'pylab notebook')
from __future__ import print_function
from charistools.modelEnv import ModelEnv
from charistools.hypsometry import Hypsometry
from charistools.convertors import Temperature2Hypsometry
tileConfigFile = '/Users/brodzik/2016_Almaty_training/modis_tiles_config.ini'
topDir = '/Users/brodzik/projects/CHARIS_FTP_copy/main_training/data'
myEnv = ModelEnv(topDir=topDir, tileConfigFile=tileConfigFile, verbose=True )


# In[2]:

drainageIDs = ["IN_Hunza_at_Danyour"] # this can be a list of drainages
years = [2001] # this can be a list of years


# In[3]:

outfile = myEnv.hypsometry_filename(drainageID=drainageIDs[0], 
                                type='temperature_by_elevation',
                                year=years[0])
print(outfile)


# In[4]:

for drainageID in drainageIDs:
    for year in years:
        outfile = myEnv.hypsometry_filename(drainageID=drainageID, 
                                            type='temperature_by_elevation', 
                                            year=year,
                                            verbose=True)
        by_elev = Temperature2Hypsometry(drainageID=drainageID,
                                         year=year,
                                         modelEnv=myEnv, 
                                         outfile=outfile, decimal_places=2,
                                         verbose=True)


# In[5]:

tsurf_by_elev = Hypsometry(filename=outfile)
tsurf_by_elev.print()


# In[6]:

fig, ax = plt.subplots()
ax = tsurf_by_elev.imshow(ax=ax, title='Hunza dscaled temperatures, 2001', 
                          cmap='RdBu_r', vmin=-40, vmax=40)
plt.show()


# Possible colormaps:
# 
# http://matplotlib.org/1.2.1/examples/pylab_examples/show_colormaps.html

# In[ ]:



