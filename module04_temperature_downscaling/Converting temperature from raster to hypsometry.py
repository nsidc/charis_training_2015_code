
# coding: utf-8

# <h2>Run temperature to hypsometries</h2>

# In[ ]:

get_ipython().magic(u'pylab inline')
from __future__ import print_function
from charistools.modelEnv import ModelEnv
from charistools.hypsometry import Hypsometry
from charistools.convertors import Temperature2Hypsometry
pylab.rcParams['figure.figsize'] = (12.0, 8.0)


# In[ ]:

tileConfigFile = '/Users/brodzik/2016_Almaty_training/modis_tiles_config.ini'
topDir = '/Users/brodzik/projects/CHARIS_FTP/2016_Almaty/main_training/data'
myEnv = ModelEnv(topDir=topDir, tileConfigFile=tileConfigFile, verbose=True )


# In[ ]:

drainageIDs = ["IN_Hunza_at_Danyour"] # this can be a list of drainages
years = [2001] # this can be a list of years


# In[ ]:

get_ipython().magic(u'cd /Users/brodzik/projects/CHARIS_FTP/2016_Almaty/main_training/data/temperature/h23v05')
get_ipython().magic(u'ls')


# In[ ]:

print(myEnv.hypsometry_filename(drainageID=drainageIDs[0], type='temperature_by_elevation', year=years[0]))


# In[ ]:

for drainageID in drainageIDs:
    for year in years:
        outfile = myEnv.hypsometry_filename(drainageID=drainageID, 
                                            type='temperature_by_elevation', year=year)
        print("Next:" + outfile)
        by_elev = Temperature2Hypsometry(drainageID=drainageID,
                                         year=year,
                                         modelEnv=myEnv, outfile=outfile, decimal_places=2,
                                         verbose=True)


# In[ ]:

print(outfile)


# In[ ]:

tsurf_by_elev = Hypsometry(filename=outfile)
tsurf_by_elev.print()


# In[ ]:

fig, ax = plt.subplots(nrows=1, ncols=1)
plt.imshow(tsurf_by_elev.data, aspect='auto' )
plt.show()


# Possible colormaps:
# 
# http://matplotlib.org/1.2.1/examples/pylab_examples/show_colormaps.html

# In[ ]:

fig, ax = plt.subplots(nrows=1, ncols=1)
plt.imshow(tsurf_by_elev.data, aspect='auto', cmap='RdBu_r', vmin=-40, vmax=40)
plt.show()


# In[ ]:

help(plt.imshow)


# In[ ]:



