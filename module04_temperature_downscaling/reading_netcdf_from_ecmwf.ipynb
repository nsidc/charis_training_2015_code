{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Working with reanalysis data and netCDF files"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "NetCDF (Network Common Data Form) is a common data format used in the atmospheric sciences.  Many global and regional climate modelling, numerical weather prediction and atmospheric reanalysis groups provide model output in NetCDF.  If you want to use this kind of data, you need to be familiar with reading data in this format.\n",
    "\n",
    "NetCDF is a set of software libraries and self describing, machine independent data formats used to create, access and share array-like data.  Array-like data can include grids such as gridded meteorological variables, time-series of grids (think a cube) and time series like temperature at a station.  This means that a well formatted netCDF file will contain geographic and time coordinates, and information such as descriptions and units for variables in the file.  The following tutorial is based on the tutorial for the netcdf4 python module.  This module is available in the Anaconda distribution.\n",
    "\n",
    "https://netcdf4-python.googlecode.com/svn/trunk/docs/netCDF4-module.html\n",
    "\n",
    "There are two versions of NetCDF formats; NetCDF3 and NetCDF4.  The netcdf4 module reads and writes both.  NetCDF4 provides many more capabilities, including compression.  However, you don't need to worry about the differences too much at this stage."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Load the netCDF4 module"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For this tutorial we will use the Dataset constructor.  So we just import this tool from the module in the normal pythonic way."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from netCDF4 import Dataset"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Set the variable root_dir to the location of the training data.  Mary Jo showed you another way to do this using CHARIS tools.  If you want to modify this notebook to use that method, go ahead."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "root_dir =\"C:/Users/apbarret/Documents/CHARIS/Almaty\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Opening a netCDF file"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To open a netCDF file, you simply use the Dataset constructor.  The first argument is filename.  Here I use a variable ncfile.  The second argument is the access mode. File can be opened for read (r) or write (w). Here, the file is opened for read access (r).  The Dataset constructor returns a Dataset instance, similar to a filehandle.  Here, I assign the Dataset instance to the variable f.\n",
    "\n",
    "A quick way to get information about variables in the file, dimensions of those variables, etc, is to print the variable f.  For the example file, this returns:\n",
    "\n",
    "    file format, NetCDF3;\n",
    "    file convention, Climate Forecast (CF) convention version 1.6;\n",
    "    history, which gives files and processing steps used to create this file;\n",
    "    dimensions, longitude, latitude, level and time;\n",
    "    variables and their data types;\n",
    "    and groups, there are no groups in this file.\n",
    "    \n",
    "Note that some of the variables have the same names as dimensions; e.g. longitude, latitude, level and time.  These are coordinate variables.  The variable t is a simple variable with time, level, latitude and longitude variables.\n",
    "\n",
    "Finally, we close the Dataset instance using the close method.  This is important to do, otherwise you can use up a lot of memory very quickly, especially if you are opening a lot of files."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "<type 'netCDF4._netCDF4.Dataset'>\n",
      "root group (NETCDF3_64BIT data model, file format NETCDF3):\n",
      "    Conventions: CF-1.6\n",
      "    history: 2016-05-10 21:18:43 GMT by grib_to_netcdf-1.14.5: grib_to_netcdf /data/data01/scratch/_mars-atls17-95e2cf679cd58ee9b4db4dd119a05a8d-clClwc.grib -o /data/data01/scratch/_grib2netcdf-atls19-95e2cf679cd58ee9b4db4dd119a05a8d-vMfX2n.nc -utime\n",
      "    dimensions(sizes): longitude(8), latitude(5), level(30), time(120)\n",
      "    variables(dimensions): float32 \u001b[4mlongitude\u001b[0m(longitude), float32 \u001b[4mlatitude\u001b[0m(latitude), int32 \u001b[4mlevel\u001b[0m(level), int32 \u001b[4mtime\u001b[0m(time), int16 \u001b[4mt\u001b[0m(time,level,latitude,longitude)\n",
      "    groups: \n",
      "\n"
     ]
    }
   ],
   "source": [
    "ncfile = root_dir+\"/\"+\"downscaling_input/era_interim.t.20010601-20010630.example.nc\"\n",
    "f = Dataset(ncfile,\"r\")\n",
    "print f\n",
    "f.close()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Reading a variable"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We'll use the same file to read the variable t and get information about this file.  Use the Dataset instance again to open the file.  A variable instance is obtained using the variables method.  Each variable will have a set of attributes that contain information about the variable.  Variable instances, themselves, also have a number of variables that describe the variable data type, number of dimensions, names of dimensions and the shape of the variable.  These can be accessed as follows. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The simple way to get information about a variable is to print it"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "<type 'netCDF4._netCDF4.Variable'>\n",
      "int16 t(time, level, latitude, longitude)\n",
      "    scale_factor: 0.00134078264437\n",
      "    add_offset: 261.339524311\n",
      "    _FillValue: -32767\n",
      "    missing_value: -32767\n",
      "    units: K\n",
      "    long_name: Temperature\n",
      "    standard_name: air_temperature\n",
      "unlimited dimensions: time\n",
      "current shape = (120, 30, 5, 8)\n",
      "filling off\n",
      "\n"
     ]
    }
   ],
   "source": [
    "f = Dataset(ncfile,\"r\")\n",
    "print f.variables['t']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "From this we can see that the variable t is a 16-bit integer with four dimensions; time, level, latitude and longitude.  Time, latitude and longitude are self explanatory.  We'll get to level later.  The variable also has a number of attributes, which include units, the long_name for the variable (Temperature) and the CF standard variable name (air_temperature).  \\_FillValue and missing_value are essentially the same things; they are values used where there is no valid data.\n",
    "\n",
    "scale_factor and add_offset are used so that the data can be stored as a smaller file.  We'll get to these in a little while.\n",
    "\n",
    "You can also see that the time dimension is an \"unlimited dimension\".  This means that you can add data to t and the size of the variable can increase along the time dimension.  For example, the \"current shape\" shows that the size of the time dimension is 120 (4 observations x 30 days).  If we wanted we can add another day (another 4 observations) to make the time dimension 124.\n",
    "\n",
    "The \"filling off\" indicates that the variable was not automatically filled when it was created.\n",
    "\n",
    "The attributes above are standard.  All variables should include \\_Fill_Value, units and long_name.  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In most applications you will not be accessing the file in interactive mode but accessing the file within a python script.  You may want to write attributes to variables.  Below, I show how to do this using the ncattrs and getncattr methods."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "scale_factor 0.00134078264437\n",
      "add_offset 261.339524311\n",
      "_FillValue -32767\n",
      "missing_value -32767\n",
      "units K\n",
      "long_name Temperature\n",
      "standard_name air_temperature\n",
      "dtype int16\n",
      "ndims 4\n",
      "dimensions (u'time', u'level', u'latitude', u'longitude')\n",
      "shape (120, 30, 5, 8)\n"
     ]
    }
   ],
   "source": [
    "t = f.variables['t']\n",
    "for ncatt in t.ncattrs():\n",
    "    print ncatt, t.getncattr(ncatt)\n",
    "print \"dtype\", t.dtype\n",
    "print \"ndims\", t.ndim\n",
    "print \"dimensions\", t.dimensions\n",
    "print \"shape\", t.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The command t.ncattrs() gets a list of attributes for the variable t.  Type t.ncattrs() below to see what this returns.  You'll need to open the file again."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[u'scale_factor',\n",
       " u'add_offset',\n",
       " u'_FillValue',\n",
       " u'missing_value',\n",
       " u'units',\n",
       " u'long_name',\n",
       " u'standard_name']"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "t.ncattrs()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The u'' indicates that the attribute names are written in unicode, which allows a consistent encoding and representation of text.  You don't need to worry about it.  Go to https://en.wikipedia.org/wiki/Unicode if you want to find out more."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now use t.getncattr() to get an attribute value.  Hint: don't forget to enclose the attribute name in quotes ''."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also access an attribute in the following way."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Units: K\n"
     ]
    }
   ],
   "source": [
    "print \"Units: \" + t.units"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "f.close()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Accessing the data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So let's get some data from the NetCDF file.  To start with, we will access the temperature data.  We do this as we did above, open the Dataset instance and get a variable instance for t.  To start with, we will find the data type of the variable."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "int16\n",
      "(120, 30, 5, 8)\n"
     ]
    }
   ],
   "source": [
    "f = Dataset(ncfile,\"r\")\n",
    "t = f.variables['t']\n",
    "print t.dtype\n",
    "print t.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As above, we find that the variable t is a 16-bit integer.  We also know, that the data has a shape (120,26,5,8).  We would like to look at some of this data but we do not want to see all 124800 data values.  We can using slicing to access part of the data.\n",
    "\n",
    "To look at values for all levels at the first timestep and first latitude and longitude grid point, we use the following command.  Here, I write the \"slice\" to a new variable tair and see what I get. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([ 227.29437141,  226.28208051,  225.72431493,  226.94442713,\n",
       "        229.67962373,  233.14554687,  236.76029687,  240.25839879,\n",
       "        243.51247827,  246.47560792,  249.19471512,  251.76633623,\n",
       "        254.15963325,  256.35181287,  258.36566841,  260.20656298,\n",
       "        261.86377032,  263.39762567,  264.58019596,  265.45572703,\n",
       "        266.02287809,  266.39963801,  266.05773844,  267.61036474,\n",
       "        268.87338199,  269.71271192,  270.22355011,  268.76746016,\n",
       "        268.9498066 ,  267.15852099])"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "tair = t[0,:,0,0]\n",
    "tair"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can check that the values make sense, i.e. are they in the expected range for air temperatures?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Min: 225.724314929  Max: 270.223550112\n"
     ]
    }
   ],
   "source": [
    "print \"Min: \"+str(min(tair))+\"  Max: \"+str(max(tair))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<image src=\"files/Comparison_International_Standard_Atmosphere_space_diving.svg.png\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "!['The International Standard Atmosphere'](files/Comparison_International_Standard_Atmosphere_space_diving.svg.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Tropospheric temperatures are between 290 K at the ground and 210 K at the tropopause so we appear to have data that makes sense."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Interestingly, the data type of the data looks to be some type of floating point value; it has decimal places.  However, when we queried the variable instance, we found that it was a 16-bit integer.\n",
    "\n",
    "Confirm that the data type of the variable instance tair is a float.  What type of float?  Why are the two types different?  What has happened?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Above we found that t had attributes add_offset and scale_factor.  Using scale_factor and add_offset allow the data to be \"packed\" as a smaller data type.  In this case a 16-bit integer (2 bytes).  By comparison, a floating point number has 32-bits (4 bytes).  I will not go into this further but you can read about it here\n",
    "\n",
    "http://www.unidata.ucar.edu/mailing_lists/archives/netcdfgroup/2002/msg00034.html\n",
    "\n",
    "The important thing to note is that the variable instance is converted (using add_offset and scale_factor) from 16-bit integer to floating-point automatically when it is read to a variable.  Not all netcdf tools do this, so it is worth checking.\n",
    "\n",
    "If your tool doesn't do this.  All you need to do is\n",
    "\n",
    "    varFloat = add_offset + (scale_factor * varInt16)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Coordinate Variables"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can access the temperature variable but we still don't have information about the time, location, or level of the data.\n",
    "\n",
    "Coordinate variables can be accessed in the same way as other variables.\n",
    "\n",
    "Print the variable instances for latitude, longitude, level and time to get their attributes and properties"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Latitude and Longitude are as expected. We can assign these two coordinates to variables as follows.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "<type 'netCDF4._netCDF4.Variable'>\n",
      "<type 'netCDF4._netCDF4.Variable'>\n"
     ]
    }
   ],
   "source": [
    "lat = f.variables['latitude']\n",
    "lon = f.variables['longitude']\n",
    "print type(lat)\n",
    "print type(lon)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The variable types are netCDF4 variables.\n",
    "\n",
    "The variables can also be referenced in the way we referenced temperature."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "<type 'numpy.ndarray'>\n",
      "<type 'numpy.ndarray'>\n"
     ]
    }
   ],
   "source": [
    "latn = f.variables['latitude'][:]\n",
    "lonn = f.variables['longitude'][:]\n",
    "print type(latn)\n",
    "print type(lonn)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When referenced this way, you get a NumPy array.  The difference is important because NetCDF4 variables can be accessed in slightly different ways than NumPy arrays.  We'll get to this later."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For now, we'll just print the variables to see the locations of the grid points."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[ 37.5   36.75  36.    35.25  34.5 ]\n",
      "[ 72.    72.75  73.5   74.25  75.    75.75  76.5   77.25]\n"
     ]
    }
   ],
   "source": [
    "print lat[:]\n",
    "print lon[:]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Time in many atmospheric datasets can be a little tricky but with the right (Python :) ) tools, they can be interpretted easily.  Looking at the time units again..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "hours since 1900-01-01 00:00:0.0\n"
     ]
    }
   ],
   "source": [
    "time = f.variables['time']\n",
    "print time.units"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "You see that they are 'hours since' some base time rather than something like year-month-day hours:minutes. \n",
    "\n",
    "Printing the first few time stamps gives the following."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1011696 1011702 1011708 1011714 1011720 1011726 1011732 1011738 1011744\n",
      " 1011750]\n"
     ]
    }
   ],
   "source": [
    "print time[0:10]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The timestamps can be interpretted into a familiar time format using the num2date tool in the netCDF4 module."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "2015-06-01 00:00:00\n",
      "2015-06-01 06:00:00\n",
      "2015-06-01 12:00:00\n",
      "2015-06-01 18:00:00\n",
      "2015-06-02 00:00:00\n",
      "2015-06-02 06:00:00\n",
      "2015-06-02 12:00:00\n",
      "2015-06-02 18:00:00\n",
      "2015-06-03 00:00:00\n",
      "2015-06-03 06:00:00\n"
     ]
    }
   ],
   "source": [
    "from netCDF4 import num2date\n",
    "import datetime\n",
    "dates = num2date(time[0:10],units=time.units,calendar=time.calendar)\n",
    "#print \"Dates corresponding to times:\\n\",dates.strftime()\n",
    "for dt in dates:\n",
    "    print dt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can see that the first few grids in the file contain data for 1 through 3 June for 6h intervals.  We will work with these in the next part of this module. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Don't forget to close the file!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "f.close()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.11"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
