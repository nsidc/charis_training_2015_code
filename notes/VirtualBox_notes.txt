
For Adjusting VB window size:

- Start the VM
- and login to the machine
- Devices->Insert Guest Additions CD image
- Start->Computer, select "VirtualBox Guest Additions CD"
- VBoxWindowsAdditions, follow prompts in installation app,
  (check box for "Always trust Oracle" to reduce no. of prompts)
- Reboot and resizing should now work
- Go to little icon at bottom of Windows desktop that looks like a CD,
  and unmount the VirtualBox CD drive.


To Mount OSX home directory for visibility inside Windows:
- Go to little icon at bottom of Windows desktop that looks like a grey
folder: do "Shared Folder Settings"
- Select Folder with + sign to add new shared folder
- Set folder path to "other" and set to home directory or wherever...
- Click "Automount" and "Make Permanent"
- Click OK
- Log out and Log back in
- E: drive to home directory should now be visible under "Computer" in
Folder Manager.

To allow clipboard between OSX and Win machine:

- Devices->Shared Clipboard, select "Bidirectional" (remember this is
  Cmd-? on OsX and Cntl-? on Windows)  

On a Windows machine, when installing Anaconda python, don't use
IE to download Anaconda, it seems to fail with errors about .js
files.

Instead, use the alternate option, to download anaconda.zip
files, save and open these and follow instructions for
installation of Anaconda.

