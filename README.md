# Training workshop for CHARIS, Almaty, 2016

The files in this repo pertain to the CHARIS training workshop to be held
in Almaty, Kazakhstan, May 2016.  It includes Python code for doing
melt modeling, iPython notebooks containing illustrative examples, as well
as utility scripts for transforming data (e.g. reprojection, resampling).

# Contacts

  * Bruce Raup (braup@nsidc.org)
  * Mary Jo Brodzik (brodzik@nsidc.org)
  * Karl Rittger (karl.rittger@nsidc.org)
  * Andy Barrett (apbarret@nsidc.org)
