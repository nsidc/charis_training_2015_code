
# coding: utf-8

# <h1>The charistools readers</h1>
# 
# <h2>Purpose of this notebook:</h2>
# 
# <p>This notebook will help you:</p>
# 
# <ol>
#   <li> read and display some fixed input raster data</li>
#   <li> read and display some forcing input raster data</li>
#   <li> add annotations to image plots and save the figures as .pngs</li>
#   <li> get simple metadata from netCDF and HDF5 files</li>
# </ol>
# 
# <p>At the end of this lesson, you should be able to open and visualize any of the raster  data that we have provided for the training.  You will be able to compare data from different dates, change the image color map to something more interesting, and annotate your image and create figures and notes like this:</p>
# 
# <img src="../images/modice_sample_h23v05.png" height="400">

# </p>Begin by initializing the ModelEnv to point to files on your system, and get the filename for MODICE data for h24v05:</p>
# 

# In[ ]:

get_ipython().magic(u'pylab notebook')
import matplotlib.pyplot as plt

from charistools.modelEnv import ModelEnv
configFile = '/Users/brodzik/2016_Almaty_training/modis_tiles_config.ini'
topDir = '/Users/brodzik/projects/CHARIS_FTP_copy/main_training/data'
myEnv = ModelEnv(tileConfigFile=configFile, topDir=topDir, verbose=True)


# In[ ]:

modiceFile = myEnv.fixed_filename(type='modice_min05yr', tileID='h24v05')
modiceFile


# <p>The python API for reading netCDF files (extension .nc) is called Dataset.  You can use it to get information from the file:</p>

# In[ ]:

from netCDF4 import Dataset


# In[ ]:

f = Dataset(filename=modiceFile, mode='r', format='NETCDF4')
f


# <p>This is a quick way to look at file-level metadata for format='NETCDF4' and 'HDF5' files. Note that there are 3 variables in this file, modice_min_year_mask, latitude and longitude.  Also there is a comment that says that mask locations with value 2 indicate MODICE for >= 5 years. </p>
# 
# <p>Remember to close the Dataset pointer "f":</p>

# In[ ]:

f.close()


# <p>The charistools readers module has a simple routine for reading raster files.</p>

# In[ ]:

from charistools.readers import read_tile
help(read_tile)


# <p>It will read one 2400x2400 array of data from selected file formats into a numpy ndarray, which you can think of as a 2D matrix.</p>

# In[ ]:

modice = read_tile(filename=modiceFile, varname='modice_min_year_mask')


# In[ ]:

print(type(modice))
print(modice.shape)


# In[ ]:

import numpy as np
print(np.amin(modice), np.amax(modice))


# <p>To display this image, we can use the matplotlib imshow method:</p>

# In[ ]:

#help(plt.imshow)


# In[ ]:

fig, ax = plt.subplots(1,1)
ax.imshow(modice,interpolation='none',cmap=plt.get_cmap('gray'))


# <p>(Use the plot controls to zoom in to an interesting area.)</p>
# 
# <p>Add an annotation and arrow to the figure, and save it</p>

# In[ ]:

ax.annotate('something interesting',
            xy=(700,850),  # data coordinate x, y for arrow to point to
            xytext=(0.05, 0.05),    # fraction of figure from bottom left 0.0, 0.0
            textcoords='figure fraction',
            arrowprops=dict(facecolor='red', shrink=0.05),
            color='r')
outfile = '/Users/brodzik/tmp/training/modice_sample_h24v05.png'
fig.savefig(outfile)


# In[ ]:

# Use the help on annotate to play with other ways to control the annotation:
#help(ax.annotate)


# <p>Some of the CHARIS forcing data (snow, albedo, temperatures) contain more than 1 tile of data, they actually contain a whole year of data in 2400x2400x365 (or 366) tile "cubes".</p>
# 
# <p>I've written a charistools reader class called ModisTileCube for reading these files.</p>

# In[ ]:

from charistools.readers import ModisTileCube
help(ModisTileCube)


# <p>The idea is that you initialize the class on a cube file, and then call the read function for the day of year that you want to read.  This saves memory, and means you don't have to read the whole year in at once.</p>
# 
# <p>Just like we did for the MODICE netCDF file, we'll need to get the variable name to read from the file:</p>
# 

# In[ ]:

snowFile = myEnv.forcing_filename(type='mod10a1_gf', tileID='h23v05', year=2004)
snowFile


# In[ ]:

from netCDF4 import Dataset
f = Dataset(snowFile, mode='r', format='HDF5')
f


# In[ ]:

f.groups['500m']


# In[ ]:

f.close()


# <p>Now we have enough information to read the 'fsca' variable from this file for a couple of days in this year:</p>

# In[ ]:

snowCube = ModisTileCube(filename=snowFile, varname='fsca')


# In[ ]:

snowTile1 = snowCube.read(doy=1)
snowTile90 = snowCube.read(doy=90)


# In[ ]:

snowTile1.shape


# <p>And we can display the data using imshow:</p>

# In[ ]:

fig, ax = plt.subplots(1,2, figsize=(8.0,4.0))
ax[0].imshow(snowTile1,interpolation='none',cmap=plt.get_cmap('gray'))
ax[0].set_title('Snow on Day of Year 1')
ax[1].imshow(snowTile90,interpolation='none',cmap=plt.get_cmap('gray'))
ax[1].set_title('Snow on Day of Year 90')


# <p>matplotlib comes with many predefined color maps:</p>
# 
# http://matplotlib.org/1.2.1/examples/pylab_examples/show_colormaps.html
# 
# <img src="../images/show_colormaps.hires.png" height="400" width="400">
# 
# <p>Exercise:
# <ol>
# <li>Try changing "gray" in the imshow commands to one that you prefer.</li>
# <li>Try looking at snow for a different pair of dates, or set up the plot for 4 images.</li>
# <li>What do you think "interpolation='none'" does?  [Try displaying the same day of data, one one side with the interpolation='none' and on the other side without it.]</li>
# <li>Add an annotation to a plot you find interesting, and save it as a .png</li>
# </ol>
# 

# In[ ]:



