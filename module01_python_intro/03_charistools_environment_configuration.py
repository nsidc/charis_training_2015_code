
# coding: utf-8

# <h1>The charistools environment configuration</h1>
# 
# <h2>Purpose of this notebook:</h2>
# 
# <p>This notebook will help you:</p>
# 
# <ol>
#   <li> edit the modis_tile_config.ini file to point to CHARIS data on your computer</li>
#   <li> use the charistools.modelEnv to point to CHARIS input data files on your system</li>
#   <li> make your own customizations to the file configuration</li>
# </ol>
# 
# <p>At the end of this lesson, you should be able to point your notebooks to 
# CHARIS files on your computer, get fixed filenames and forcing filenames, and set your own values for customized locations you may want to use.</p>
# 

# <img src="../images/charistools.modelEnv.png" width=500>

# </p>We have compiled a selection of MODIS snow, albedo and ice data and reanalysis temperature data for the CHARIS project.  The first step is to copy the data to your hard drive, and go to the data location on your system.</p>
# 
# <p>In Jupyter notebooks, they have a "magic" operator, the percent sign, %, that you can use to change directories and list directory contents.  Using the '%' as the first character of a line in the notebook, you are telling python to "drop" to the operating system and running shell commands.</p>
# 

# <h3>The CHARIS training data</h3>

# In[ ]:

# Your top level data directory will be different from mine:
# On my Mac (or linux):
get_ipython().magic(u'pwd')
get_ipython().magic(u'cd /Users/brodzik/projects/CHARIS_FTP_copy/')
get_ipython().magic(u'ls')

# On Windows: forward slashes are OK, but you will probably have to use 'C:' or 'E:' 
# depending on the location of your training data
#%pwd
#%cd E:/projects/CHARIS_FTP_copy
#%ls


# From the thumbdrives, the data are in "main_training/data", and I want to go there:

# In[ ]:

get_ipython().magic(u'cd main_training/data')
get_ipython().magic(u'ls')


# <p>The main data directory and each subdirectory includes a file called "00notes.txt".</p>
# 
# <p>Each one contains a short description of the data included there, with filename conventions and description of the contents.</p>
# 

# In[ ]:

get_ipython().magic(u'more 00notes.txt')


# In[ ]:

get_ipython().magic(u'ls basin_masks')


# <p>And so on for the other data directories.  </p>
# 
# <p>Read the 00notes file to find out more about basin_masks:</p>

# In[ ]:

get_ipython().magic(u'more basin_masks/00notes.txt')


# <p>I have written a charistools class called "ModelEnv" to (hopefully) keep track of these names for you.</p>
# 
# <p>"import" the ModelEnv class and look at the help for this class to learn more:</p>

# In[ ]:

from charistools.modelEnv import ModelEnv
help(ModelEnv)


# <p>The ModelEnv class needs a text file that describes the CHARIS filenames.</p>
# 
# <p>I have stored this file at the top level of the training notebooks.  It's an ASCII file that uses Windows '.ini' format to describe the directory location and filenaming convention of each of the types of data we are using.  </p>

# In[ ]:

# Mac or linux:
get_ipython().magic(u'cd /Users/brodzik/2016_Almaty_training')

# Windows:
#%cd E:/2016_Almaty_training
#%ls


# In[ ]:

get_ipython().magic(u'more modis_tiles_config.ini')


# <p>Take a minute to look at the file contents here.  For example, find the entry for "basin_masks", something like:</p>
# <pre>
# [input]
# ...
#     [[fixed]]
#         [[[basin_mask]]]
#         dir = %MODEL_TOP_DIR%/basin_masks/
#         pattern = %DRAINAGEID%.basin_mask.%TILEID%.tif
# ...
# </pre>
# 
# <p>Windows ini files give us the ability to define a hierarchy of levels in the file, so in this case, we've defined 3 levels, "input", "fixed" and "basin_mask".  All of the input files for our modelling are MODIS tile files, which are arrays of 2400x2400 pixels per tile version of the MODIS sinusoidal projection, shown here for the full globe:</p>
# 
# <img src="../images/modis_map.png">
# 
# <p> The tiles are named according to horizontal "h" and vertical "v" coordinates in the graphic.</p>
# 
# <p>Here is the same information, zoomed to the CHARIS region and reprojected into a projection that is more like what we are used to seeing: </p>
# 
# <img src="../images/himalaya_tiles.png".</p>
# 
# <p>From experience, I can tell you that the Hunza river basin spans the 2 tiles, h23v05 and h24v05.  Is there enough information and detail in the zoomed picture to see which tiles cover the basin you are studying?</p>
# 
# <p>Note that the filename pattern value for basin_mask files has the wildcard string "%TILEID%".  The ModelEnv class will help keep track of filenames and wildcard strings, so that we can read them across different operating systems.</p>

# In[ ]:

configFile = '/Users/brodzik/2016_Almaty_training/modis_tiles_config.ini'
#configFile = 'E:/2016_Almaty_training/modis_tiles_config.ini'
#help(ModelEnv)
myEnv = ModelEnv(tileConfigFile=configFile, verbose=True)


# In[ ]:

myEnv.tileConfig


# <p>So modelEnv basically reads in the contents of the .ini file into a python "dictionary".
# A dictionary is basically a lookup table with keyword=values. They can contain any values, in this case they correspond to filename directories and patterns for the model data.</p>
# 
# <p>For e.g., get the "value" of the ['input']['fixed'] entries like this:</p>
# 

# In[ ]:

myEnv.tileConfig['input']['fixed']


# In[ ]:

myEnv.tileConfig['input']['fixed']['basin_mask']


# In[ ]:

myEnv.tileConfig['input']['fixed']['basin_mask']['dir']


# In[ ]:

myEnv.tileConfig['input']['fixed']['basin_mask']['pattern']


# <p>Note that the wildcards are still here, they haven't yet been substituted for the readl values on your system.</p>
# 
# <p>Then use the modelEnv methods called 'fixed_filename' and 'forcing_filename' to make real filenames at runtime:</p>

# In[ ]:

help(myEnv.fixed_filename)


# In[ ]:

h23maskFile = myEnv.fixed_filename(type='basin_mask', 
                             drainageID='IN_Hunza_at_Danyour', 
                             tileID='h23v05', verbose=True)


# <p>But this isn't quite right, yet, because the .ini file has set the top level directory to a default value of '~/projects/CHARIS_FTP_COPY', which doesn't match where you copied the 
# data to your system.  This value can also be set from the notebook, by using the topDir argument to the ModelEnv initilizer, like this:</p>

# In[ ]:

topDir = '/Users/brodzik/projects/CHARIS_FTP_copy/main_training/data'
#topDir = 'E:/projects/CHARIS_FTP_copy/main_training/data'
myEnv = ModelEnv(tileConfigFile=configFile, topDir=topDir, verbose=True)


# In[ ]:

h23maskFile = myEnv.fixed_filename(type='basin_mask', 
                             drainageID='IN_Hunza_at_Danyour', 
                             tileID='h23v05', verbose=True)


# <p>Try to get filenames for the following input files:</p>
# <ol>
# <li>Gap-filled snow data from MOD10A1 for h23v05 for 2004 (model forcing data)</li>
# <li>MODICE data for 1 strike, h24v05 (model fixed data)</li>

# In[ ]:




# <p>These should be the answers:</p>

# In[ ]:

#print(myEnv.forcing_filename(type='mod10a1_gf', tileID='h23v05', year=2004))
#print(myEnv.fixed_filename(type='modice_min05yr', tileID='h24v05'))


# <p>Note that you can add your own customized values to the .ini configuration file, too.  For example, you can make a directory for training output files, set it as a customized value in the .ini file, and then use it in your output filenames.</p>
# 
# <p>Try adding a line like this to the bottom of your copy of modis_tile_config.ini:</p>
# 
# <pre>
# [output]
# dir = E:/tmp/training/
# </pre>
# 
# <p>Then re-read the config file and get the value back:</p>

# In[ ]:

myEnv = ModelEnv(tileConfigFile=configFile, topDir=topDir, verbose=True)
myEnv.tileConfig['output']


# In[ ]:



