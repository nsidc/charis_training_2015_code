
# coding: utf-8

# <h1>Introduction to Ipython Notebooks</h1>

# <h2>N.B. Ipython notebooks are being replaced by "Jupyter" notebooks.  Depending on the version of python and ipython you have installed, you may have to replace "ipython" with "jupyter" when starting the server.
# 
# <h2>Purpose of this notebook:
# 
# This notebook will introduce you to:     
# 
# <ol>
#   <li> the basic features of interactive python that you use from a web browser</li>             <li> numpy (http://www.numpy.org), which you need to do scientific analysis in python</li>     <li> matplotlib (http://www.matplotlib.org), which you need to make plots and figures in python</li>
#   <li> ways to get help while learning
# </ol>
# 
# <p>At the end of this lesson, you should be able to display a plot of $y = x^2$.</p>
# 
# </h2>

# This notebook has:
# 
# a title (that can be renamed by clicking on it)  <i>Try doing this now!</i>
# 
# a main menu bar with functions for adding content, 
# 
# and a toolbar with often-used tools.  Try the Help->User Interface Tour to see each of these parts pointed out to you.
# 
# Here is more stuff.
Here is some raw text.
# Notebooks consists of "cells" that can contain markdown, comments, executable code snippets and more.
# 
# "Execute" a cell by entering Shift-Return 
# 
# or clicking Cell->Run on the menu
# 
# or clicking the play button on the toolbar.

# In[ ]:

print("Hello world\n")


# In a cell, you can move the cursor around with:
# 
# <ul>
#     <li>Ctrl-a : beginning of line
#     <li>Ctrl-e : end of line
#     <li>Ctrl-k : cut to end of line
#     <li>Ctrl-p : go to previous line
#     <li>Ctrl-n : go to next line
# </ul>
# 

# Python has a notion of "namespaces," which requires you to "import" modules that are not available as a default.  One of the most important of these for scientific computing is numpy (Numerical Python http://www.numpy.org/).  While python has some basic types for floats, ints, lists and dicts (structures), if you are doing anything with vectors or matrices of numbers, you should be using numpy.  Here's an example:

# In[ ]:

import numpy as np
x = np.array(  [ [ 1, 2 ], [ 3, 4 ] ] )
x


# In[ ]:

print(x)


# A handy feature while learning about something new in python is to call the help function on it (you can do this on objects, variables, or functions to get more information about them).

# In[ ]:

help(x)


# As you learn, remember that Google is a great resource, when you are trying to figure something out, "python how to ..." is remarkably effective for finding answers.
# 
# Also googling an error message will often bring up answers from someone else who's doing the same thing you are.

# Another very valuable python module that we plan to use is called matplotlib (http://matplotlib.org/).  It is a python module with syntax very similar to that of matlab.  So if you are a matlab user, you will be very comfortable with it!  (I am not a matlab user, I know IDL a lot better, so I struggle with it a little bit.)  
# 
# But first, here's a numpy function for generating a sequence of N numbers from zero to N-1:

# In[ ]:

help(np.arange)


# In[ ]:

# You only need to execute this command once, 
# at the beginning of your notebook session
get_ipython().magic(u'pylab notebook')
import matplotlib.pyplot as plt
plt.plot(np.arange(150))
#plt.show()  # this line may not be needed (depends on matplotlib.is_interactive() setting


# In the plot window of y=x that we just made:
# 
# <ol>
# <li> Use the little icon in the lower right part of the plot to resize it interactively,
# <li> Use "square" icon to zoom in to an area of the plot,
# <li> Explore the other icons for right-arrow, left-arrow, etc
# </ol>
# 
# Hovering over an icon will bring up a "tooltip".

# matplotlib lets you make multiple plots in a single figure.  Here we set up a figure for 2 side-by-side plots, and add data to each one.
# 

# In[ ]:

x = np.arange(150)
y = x
fig, ax = plt.subplots(1,2)

ax[0].set_title("Left title")
ax[0].plot(y)

ax[1].set_title("Right title")
ax[1].plot(-1 * y)

plt.suptitle("Main title")


# In an ipython notebook like the one we are using here, there are a lot of options for displaying text, notes and images.  Just one more example (because it's so cool), is that you can display latex-formatted equations very easily if you are using math equations.  Like this: 

# ## $$k_{0} = \cos{\phi_{1}} { \sqrt{ 1 - e^2\sin^2{\phi_{1}}}}$$

# <h2>
# Further examples and tutorial on jupyter notebooks: <br>
# http://jupyter-notebook-beginner-guide.readthedocs.io/en/latest/execute.html
# </h2>

# <h2>
# Numpy on-line:
# http://numpy.org
# </h2>

# <h2>
# Matplotlib documents:
# http://matplotlib.org
# </h2>

# The python syntax for an exponent is the double-asterisk ** operator.
# 
# Try using the command cell below this to display 2 plots, one on top of the other, of $y = x$ and $y = x^2$.  Give it a main title like this:  "&lt;your name&gt;'s first python plot".
# 
# 

# In[ ]:



