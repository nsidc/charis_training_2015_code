
# coding: utf-8

# <h2>Installing CHARIS project tools</h2>
# 

# <h2>Purpose of this notebook:</h2>
# 
# <p>This notebook will help you:</p>
# 
# <ol>
#   <li> install charistools from the cloud</li>
#   <li> get help on the charistools (or any other library you import)
# </ol>
# 
# <p>At the end of this lesson, you should be able to import charistools, and retrieve the names of files on your machine.</p>
# 
# 

# <p>We are developing a set of python utilities for the CHARIS project as an open source project on bitbucket.org, here:</p>
# 
# http://bitbucket.org/nsidc/charistools
# 
# <p>If you want to get direct access to the source code on bitbucket.org, you will need to create a (free) bitbucket account.</p>
# 
# <p>You can get access to the charistools as a conda package to run on your system from here:</p>
# 
# http://anaconda.org/nsidc/charistools
# 
# <p>(You will need a network connection to do the following steps.)</p>

# <h3>Install the charistools conda package on your system</h3>

# <h3>1. Use the conda package manager to verify that you are running python 2.7</h3>
# 
# <table>
#     <tr>
#         <th>Windows</th>
#         <th>OSX/linux</th>
#     </tr>
#     <tr>
#         <td>Open a "Cmd" windows command-line tool</td>
#         <td>Open a Terminal</td>
#     </tr>
# </table>
# 
# <p>And run:
# <pre>
# python --version                                                          
# </pre>
# 
# -> should return "Python 2.7.x" for some recent value of x
# 
# <p>If you are running python version 3, you will need to install python 2 to proceed with the rest of the exercises.</p>

# <h3>2. Use conda to see what you already have installed</h3>
# <pre>
# conda list
# </pre>
# 
# -> should return a list of packages that Anaconda would have installed for you
# 
# 

# <h3>3. Add the nsidc conda channel to the places where conda looks for packages</h3>
# <pre>
# conda config --get-channels
# </pre>
# 
# -> may return nothing, or other channels you may have added
# 
# <pre>
# conda config --add channels https://conda.anaconda.org/nsidc
# </pre>  
# 
# -> this adds the NSIDC conda "channel" to the list of places to go looking for conda packages                                
#                                                                                 
# <pre>
# conda config --get channels
# </pre>
# 
# -> should now return a list that includes these: 
# <samp>
#          --add channels 'defaults'                                              
#          --add channels 'https://conda.anaconda.org/nsidc'                      
# </samp>
# 
# <i>The following should not normally be needed, but is a workaround for a bug in the conda package.</i>
# 
# <p>Install a package that the charistools depend on:</p>
# 
# <pre>
# conda install -c scitools shapely
# </pre>
# 
# <p>Verify that shapely is now installed, look for it in the output to</p>
# 
# <pre>
# conda list
# </pre>

# <h3>4. Install charistools</h3>
# <pre>
# conda install charistools
# </pre>
# 
# <p>(Follow the prompts to install charistools on your system)

# <h3>5. Test the charistools installation in your Jupyter notebook</h3>
# 
# <p>Start the Jupyter notebook, to get access to the newly installed charistools.  And then try to import charistools:</p>
# 
# 

# In[ ]:

import charistools


# In[ ]:

help(charistools)


# <p>Some of these modules contain "classes" and some contain collections of methods (and some contain both). </p>
# 
# <p>A "class" is a collection of data and methods that act on that data.</p>
# 
# <p>Next we will look at more detailed help on the convertors module, and in the next notebook we will learn how to use the modelEnv class.</p>

# In[ ]:

import charistools.convertors
help(charistools.convertors)


# In[ ]:

from charistools import modis_tile_bounds
help(modis_tile_bounds)


# In[ ]:



