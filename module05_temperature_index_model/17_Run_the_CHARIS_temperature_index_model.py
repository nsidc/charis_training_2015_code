
# coding: utf-8

# <h2>Purpose of this notebook:</h2>
# 
# <p>This notebook will introduce you to:
# <ol>
# <li>collecting model inputs and visualizing them as a line plot</li>
# <li>also visualizing them as 2D hypsometries</li>
# <li>running the temperature index melt model on a single surface (SOL, SOI or EGI)</li>
# <li>running the melt model on all 3 surfaces and looking at melt output</li>
# </ol>
# 
# <p>At the end of this lesson, you should be able to easily visualize the melt model inputs and outputs with the data we've provided for the Hunza</p>
# 
# <img src="../images/charistools.meltModel.png">
# <img src="../images/charistools.meltModel.IO.png">

# In[ ]:

get_ipython().magic(u'pylab notebook')
from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
from charistools.hypsometry import Hypsometry
from charistools.modelEnv import ModelEnv
configFile = '/Users/brodzik/2016_Almaty_training/modis_tiles_config.ini'
topDir = '/Users/brodzik/projects/CHARIS_FTP_copy/main_training/data'
myEnv = ModelEnv(tileConfigFile=configFile, topDir=topDir, verbose=True)


# <p>Model inputs for running the 3-surface temperature index model for the Hunza will be:</p>
# <ol>
# <li>fSCA Hypsometries for snow-on-land, snow-on-ice, exposed-glacier-ice</li>
# <li>downscaled temperature Hypsometries</li>
# </ol>
# 
# <p>I've prepared these files for you for 2001 data, in derived_hypsometries:</p>
# 
# <p>This uses the slick "$variable" syntax that we discovered yesterday!</p>

# In[ ]:

get_ipython().magic(u'cd $topDir"/derived_hypsometries"')
get_ipython().magic(u'ls')


# In[ ]:

get_ipython().magic(u'more $configFile')


# In[ ]:

help(myEnv.hypsometry_filename)


# In[ ]:

drainageID = 'IN_Hunza_at_Danyour'
year = 2001


# <p>Start by reading the 3 input surface hypsometries (SOL, SOI, EGI), and the temperature hypsometry.  These are files I've prepared for you ahead of time, but they are basically what you would have produced yesterday afternoon, when you ran Fsca2Hypsometry and Temperature2Hypsometry:</p>

# In[ ]:

SOLFile = myEnv.hypsometry_filename(type='snow_on_land_by_elevation',
                                    drainageID=drainageID, year=year)
print(SOLFile)
SOIFile = myEnv.hypsometry_filename(type='snow_on_ice_by_elevation',
                                    drainageID=drainageID, year=year,
                                    ablation_method='albedo_mod10a1',
                                    threshold=0.46)
print(SOIFile)
EGIFile = myEnv.hypsometry_filename(type='exposed_glacier_ice_by_elevation',
                                    drainageID=drainageID, year=year,
                                    ablation_method='albedo_mod10a1',
                                    threshold=0.46)
print(EGIFile)
tempFile = myEnv.hypsometry_filename(type='temperature_by_elevation',
                                    drainageID=drainageID, year=year)
print(tempFile)


# <p>The last module in charistools is called meltModels, it contains a set of methods to run the temperature index model and display inputs and outputs</p>

# In[ ]:

import charistools.meltModels
help(charistools.meltModels)


# <p>PlotTriSurfInput is a plotting routine that will create a time series of all three areas and temperature on the same Axes:</p>

# In[ ]:

from charistools.meltModels import PlotTriSurfInput


# In[ ]:

SOL_hyps = Hypsometry(filename=SOLFile)
SOI_hyps = Hypsometry(filename=SOIFile)
EGI_hyps = Hypsometry(filename=EGIFile)
temp_hyps = Hypsometry(filename=tempFile)


# In[ ]:

fig, ax = plt.subplots(figsize=(9.,4.))
ax = PlotTriSurfInput(ax, SOL_hyps=SOL_hyps,
                      SOI_hyps=SOI_hyps,
                      EGI_hyps=EGI_hyps,
                      temperature_hyps=temp_hyps)
fig.tight_layout()
#fig.savefig("/Users/brodzik/2016_Almaty_training/images/2PlotTriSurfInput.Hunza.2001.png")


# <p>This plot makes use of python's ability to make a "twin" of an axis so that you can include data with a whole different range of values on the same plot for analysis.</p>
# 
# <p>Now look again at the meltModel help, at the method called TempIndexMelt, which takes one area file and a temperature file:</p>

# In[ ]:

from charistools.meltModels import TempIndexMelt
help(TempIndexMelt)


# <p>Use this method to "melt" one type of area data.  Note that you can specify the min/max DDF.</p>

# In[ ]:

EGI_melt_hyps = TempIndexMelt(EGI_hyps, 
                              temp_hyps, min_ddf=2.0, max_ddf=7.0)


# In[ ]:

fig, ax = plt.subplots()
ax = EGI_melt_hyps.imshow(ax, title='Hunza EGI Melt, 2001', cmap='Purples')


# <p>Our implementation of the melt model uses a sine wave to vary the DDFs between the min/max values, using the function _seasonal_ddfs:</p>

# In[ ]:

help(charistools.meltModels._seasonal_ddfs)


# <p>This method is designated to be a private "helper" function by the prefix underscore in the name, but if you know it's there you can still access it.  It takes a date index like you get from the pandas data array in our Hypsometries, and returns the seasonally-adjusted DDFs that vary between the min and max you enter, with the minimum (maximum) occuring at the winter (summer) solstice, Dec 21 (June 21)</p>

# In[ ]:

EGI_hyps.data.index


# In[ ]:

ddfs = charistools.meltModels._seasonal_ddfs(date_index=EGI_hyps.data.index)
fig, ax = subplots()
ddfs.plot(ax=ax)
ax.set_ylabel('Degree Day Factor ($mm/degC/day$)')
ax.set_title('Seasonally-varying DDF')
fig.tight_layout()


# In[ ]:

#help(plot)


# <p>The actual model implementation is extremely simple, because we leverage the power of the pandas Dataframe that we've defined for all the hypsometries.  It takes care of things like missing values, and the fact that the 3 different hypsometries have different elevation band ranges.</p>
# 
# <p>Here it is:</p>
# <pre>
#    km_per_mm = 1E-6                                                                     
#     area_km2_df = area_hyps.data.copy()                                                  
#     t_C_df = temperature_hyps.data.copy()                                                
#                                                                                          
#     too_cold = t_C_df <= 0.0                                                             
#     ddf_mm_pday_pdegC = _seasonal_ddfs(area_km2_df.index,                                
#                                        min_ddf=min_ddf,                                  
#                                        max_ddf=max_ddf)                                  
#                                                                                          
#     area_km2_df[too_cold] = 0.0                                                          
#     melt_km3_df = area_km2_df.multiply(                                                  
#         t_C_df, fill_value=0.0).multiply(                                                
#         ddf_mm_pday_pdegC, axis=0) * km_per_mm                                           
#                                                                                          
#     # Return hyps object, save DDFs used in the comments                                 
#     melt_km3_hyps = Hypsometry(                                                          
#         data=melt_km3_df,                                                                
#         comments=[                                                                       
#             "Hypsometry created : " + str(dt.datetime.now()),                            
#             "Melt in km^3",                                                              
#             "min_DDF_mm_pday_pdegC : %f" % min_ddf,                                      
#             "max_DDF_mm_pday_pdegC : %f" % max_ddf])                                     
#     return melt_km3_hyps
# <pre>    
# 

# <p>Finally, charistools.meltModels has a method that calls TempIndexMelt for each of the 3 surfaces, SOL, SOI and EGI in turn, and returns 3 Hypsometries of the melt output.</p>

# In[ ]:

from charistools.meltModels import TriSurfTempIndexMelt
help(TriSurfTempIndexMelt)


# In[ ]:

(SOL_melt_hyps, SOI_melt_hyps, EGI_melt_hyps) = TriSurfTempIndexMelt(
    SOLFile=SOLFile, 
    SOIFile=SOIFile, 
    EGIFile=EGIFile, 
    temperatureFile=tempFile, 
    min_snow_ddf=2.0, max_snow_ddf=7.0, min_ice_ddf=2.0, max_ice_ddf=9.0, verbose=True)


# In[ ]:

from charistools.meltModels import PlotTriSurfMelt
fig, ax = plt.subplots(1,1,figsize=(9.,3.))
ax = PlotTriSurfMelt(ax=ax, 
                     SOL_melt_hyps=SOL_melt_hyps, 
                     SOI_melt_hyps=SOI_melt_hyps, 
                     EGI_melt_hyps=EGI_melt_hyps, 
                     title='Hunza melt, 2001', linewidth=3, verbose=False)
#fig.savefig("/Users/brodzik/2016_Almaty_training/images/PlotTriSurfMelt.Hunza.2001.png")


# <p>And here is a 3-panel version of the imshow output:</p>

# In[ ]:

from charistools.meltModels import ImshowTriSurfMelt
fig, ax = plt.subplots(3,1, figsize=(9.,8.))
ax = ImshowTriSurfMelt(axs=ax, 
                         SOL_melt_hyps=SOL_melt_hyps, 
                         SOI_melt_hyps=SOI_melt_hyps, 
                         EGI_melt_hyps=EGI_melt_hyps, 
                         verbose=False)
fig.tight_layout()
#fig.savefig("/Users/brodzik/2016_Almaty_training/images/ImshowTriSurfMelt.Hunza.2001.png")


# <h3>Exercises</h3>
# 
# <p>We included all the data you will need to run the model for 2001 and/or 2004, for tiles that cover your basins of interest.</p>
# 
# <p>You now have enough information to run the model for:</p>
# <ol>
# <li>the Hunza for 2004, and compare outputs to 2001</li>
# <li>your basin of interest for 2001 or 2004</li>
# </ol>

# In[ ]:




# In[ ]:



